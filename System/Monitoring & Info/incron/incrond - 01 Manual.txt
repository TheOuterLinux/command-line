From: https://linux.die.net/man/8/incrond

die.net

incrond(8) - Linux man page

Name
----

incrond - inotify cron (incron) daemon

Synopsis
--------

incrond [ -f file ] [ -n | -k ]

Description
-----------

The inotify cron daemon (incrond) is a daemon which monitors filesystem events
and executes commands defined in system and user tables. It's use is generally
similar to cron(8).

incrond can be started from /etc/rc, /etc/rc.local and so on. It daemonizes
itself (returns immediately) and doesn't need to be started with & and through
nohup(1). It can be run on foreground too.

incrond uses two categories of tables incrontab(5). System tables are usually
located in /etc/incron.d and are maintained outside of incron (e.g. by various
applications). These tables work on root rights level and thus any file may be
watched and commands are executed with root privileges.

User tables are located in /var/spool/incron by default and have names based on
user accounts. These tables use users' access rights, thus only files which the
user may access are watched. Commands are executed with users' privileges.

If a table (incrontab) is changed incrond reacts immediately and reloads the
table. Currently running child processes (commands) are not affected.

There are two files determining whether an user is allowed to use incron. These
files have very simple syntax - one user name per line. If /etc/incron.allow
exists the user must be noted there to be allowed to use incron. Otherwise if /
etc/incron.deny exists the user must not be noted there to use incron. If none
of these files exists there is no other restriction whether anybody may use
incron. Location of these files can be changed in the configuration.

The daemon itself is currently not protected against looping. If a command
executed due to an event causes the same event it leads to an infinite loop
unless a flag mask containing IN_NO_LOOP is specified. Please beware of this
and do not allow permission for use incron to unreliable users.

-n (or --foreground) option causes running on foreground. This is useful
especially for testing, debugging and optimization.

-k (or --kill) option terminates a running instance of incrond.

-f <FILE> (or --config=<FILE>) option specifies another location for the
configuration file (/etc/incron.conf is used by default).

Environment variables: For system tables, the default (the same as for incrond
itself) environment variable set is used. The same applies to root's table. For
non-root user tables, the whole environment is cleared and then only these
variables are set: LOGNAME, USER, USERNAME, SHELL, HOME and PATH. The variables
(except PATH) take values from the user database (e.g. /etc/passwd). The PATH
variable is set to /usr/local/bin:/usr/bin:/bin:/usr/X11R6/bin.

See Also

incrontab(1), incrontab(5), incron.conf(5)

Bugs

incrond is currently not resistent against looping. Recursive monitoring (whole
subtrees) has not been implemented yet.

Author

Lukas Jelinek <lukas@aiken.cz> (please report bugs to http://bts.aiken.cz or <
bugs@aiken.cz>).

Copying

This program is free software. It can be used, redistributed and/or modified
under the terms of the GNU General Public License, version 2.
