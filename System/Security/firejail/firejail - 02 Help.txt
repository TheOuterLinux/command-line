firejail - version 0.9.62.4

Firejail is a SUID sandbox program that reduces the risk of security breaches by
restricting the running environment of untrusted applications using Linux
namespaces.

Usage: firejail [options] [program and arguments]

Options:
    -- - signal the end of options and disables further option processing.
    --allow-debuggers - allow tools such as strace and gdb inside the sandbox.
    --allusers - all user home directories are visible inside the sandbox.
    --apparmor - enable AppArmor confinement.
    --apparmor.print=name|pid - print apparmor status.
    --appimage - sandbox an AppImage application.
    --audit[=test-program] - audit the sandbox.
    --bandwidth=name|pid - set bandwidth limits.
    --bind=dirname1,dirname2 - mount-bind dirname1 on top of dirname2.
    --bind=filename1,filename2 - mount-bind filename1 on top of filename2.
    --blacklist=filename - blacklist directory or file.
    --build - build a whitelisted profile for the application.
    --build=filename - build a whitelisted profile for the application.
    --caps - enable default Linux capabilities filter.
    --caps.drop=all - drop all capabilities.
    --caps.drop=capability,capability - blacklist capabilities filter.
    --caps.keep=capability,capability - whitelist capabilities filter.
    --caps.print=name|pid - print the caps filter.
    --cgroup=tasks-file - place the sandbox in the specified control group.
    --chroot=dirname - chroot into directory.
    --cpu=cpu-number,cpu-number - set cpu affinity.
    --cpu.print=name|pid - print the cpus in use.
    --debug - print sandbox debug messages.
    --debug-blacklists - debug blacklisting.
    --debug-caps - print all recognized capabilities.
    --debug-errnos - print all recognized error numbers.
    --debug-private-lib - debug for --private-lib option.
    --debug-protocols - print all recognized protocols.
    --debug-syscalls - print all recognized system calls.
    --debug-whitelists - debug whitelisting.
    --defaultgw=address - configure default gateway.
    --deterministic-exit-code - always exit with first child's status code.
    --dns=address - set DNS server.
    --dns.print=name|pid - print DNS configuration.
    --env=name=value - set environment variable.
    --fs.print=name|pid - print the filesystem log.
    --get=name|pid filename - get a file from sandbox container.
    --help, -? - this help screen.
    --hostname=name - set sandbox hostname.
    --hosts-file=file - use file as /etc/hosts.
    --ignore=command - ignore command in profile files.
    --interface=name - move interface in sandbox.
    --ip=address - set interface IP address.
    --ip=none - no IP address and no default gateway are configured.
    --ip6=address - set interface IPv6 address.
    --iprange=address,address - configure an IP address in this range.
    --ipc-namespace - enable a new IPC namespace.
    --join=name|pid - join the sandbox.
    --join-filesystem=name|pid - join the mount namespace.
    --join-network=name|pid - join the network namespace.
    --join-or-start=name|pid - join the sandbox or start a new one.
    --keep-dev-shm - /dev/shm directory is untouched (even with --private-dev).
    --keep-var-tmp - /var/tmp directory is untouched.
    --list - list all sandboxes.
    --ls=name|pid dir_or_filename - list files in sandbox container.
    --mac=xx:xx:xx:xx:xx:xx - set interface MAC address.
    --machine-id - preserve /etc/machine-id
    --memory-deny-write-execute - seccomp filter to block attempts to create
	memory mappings that are both writable and executable.
    --mtu=number - set interface MTU.
    --name=name - set sandbox name.
    --net=bridgename - enable network namespaces and connect to this bridge.
    --net=ethernet_interface - enable network namespaces and connect to this
	Ethernet interface.
    --net=none - enable a new, unconnected network namespace.
    --net.print=name|pid - print network interface configuration.
    --netfilter[=filename,arg1,arg2,arg3 ...] - enable firewall.
    --netfilter.print=name|pid - print the firewall.
    --netfilter6=filename - enable IPv6 firewall.
    --netfilter6.print=name|pid - print the IPv6 firewall.
    --netmask=address - define a network mask when dealing with unconfigured	parrent interfaces.
    --netns=name - Run the program in a named, persistent network namespace.
    --netstats - monitor network statistics.
    --nice=value - set nice value.
    --no3d - disable 3D hardware acceleration.
    --noblacklist=filename - disable blacklist for file or directory.
    --nodbus - disable D-Bus access.
    --nodvd - disable DVD and audio CD devices.
    --noexec=filename - remount the file or directory noexec nosuid and nodev.
    --nogroups - disable supplementary groups.
    --nonewprivs - sets the NO_NEW_PRIVS prctl.
    --noprofile - do not use a security profile.
    --noroot - install a user namespace with only the current user.
    --nosound - disable sound system.
    --noautopulse - disable automatic ~/.config/pulse init.
    --novideo - disable video devices.
    --nou2f - disable U2F devices.
    --nowhitelist=filename - disable whitelist for file or directory.
    --output=logfile - stdout logging and log rotation.
    --output-stderr=logfile - stdout and stderr logging and log rotation.
    --overlay - mount a filesystem overlay on top of the current filesystem.
    --overlay-named=name - mount a filesystem overlay on top of the current
	filesystem, and store it in name directory.
    --overlay-tmpfs - mount a temporary filesystem overlay on top of the
	current filesystem.
    --overlay-clean - clean all overlays stored in $HOME/.firejail directory.
    --private - temporary home directory.
    --private=directory - use directory as user home.
    --private-cache - temporary ~/.cache directory.
    --private-home=file,directory - build a new user home in a temporary
	filesystem, and copy the files and directories in the list in
	the new home.
    --private-bin=file,file - build a new /bin in a temporary filesystem,
	and copy the programs in the list.
    --private-dev - create a new /dev directory with a small number of
	common device files.
    --private-etc=file,directory - build a new /etc in a temporary
	filesystem, and copy the files and directories in the list.
    --private-tmp - mount a tmpfs on top of /tmp directory.
    --private-cwd - do not inherit working directory inside jail.
    --private-cwd=directory - set working directory inside jail.
    --private-opt=file,directory - build a new /opt in a temporary filesystem.
    --private-srv=file,directory - build a new /srv in a temporary filesystem.
    --profile=filename|profile_name - use a custom profile.
    --profile.print=name|pid - print the name of profile file.
    --profile-path=directory - use this directory to look for profile files.
    --protocol=protocol,protocol,protocol - enable protocol filter.
    --protocol.print=name|pid - print the protocol filter.
    --put=name|pid src-filename dest-filename - put a file in sandbox
	container.
    --quiet - turn off Firejail's output.
    --read-only=filename - set directory or file read-only.
    --read-write=filename - set directory or file read-write.
    --rlimit-as=number - set the maximum size of the process's virtual memory.
	(address space) in bytes.
    --rlimit-cpu=number - set the maximum CPU time in seconds.
    --rlimit-fsize=number - set the maximum file size that can be created
	by a process.
    --rlimit-nofile=number - set the maximum number of files that can be
	opened by a process.
    --rlimit-nproc=number - set the maximum number of processes that can be
	created for the real user ID of the calling process.
    --rlimit-sigpending=number - set the maximum number of pending signals
	for a process.
    --rmenv=name - remove environment variable in the new sandbox.
    --scan - ARP-scan all the networks from inside a network namespace.
    --seccomp - enable seccomp filter and apply the default blacklist.
    --seccomp=syscall,syscall,syscall - enable seccomp filter, blacklist the
	default syscall list and the syscalls specified by the command.
    --seccomp.block-secondary - build only the native architecture filters.
    --seccomp.drop=syscall,syscall,syscall - enable seccomp filter, and
	blacklist the syscalls specified by the command.
    --seccomp.keep=syscall,syscall,syscall - enable seccomp filter, and
	whitelist the syscalls specified by the command.
    --seccomp.print=name|pid - print the seccomp filter for the sandbox
	identified by name or PID.
    --shell=none - run the program directly without a user shell.
    --shell=program - set default user shell.
    --shutdown=name|pid - shutdown the sandbox identified by name or PID.
    --timeout=hh:mm:ss - kill the sandbox automatically after the time
	has elapsed.
    --tmpfs=dirname - mount a tmpfs filesystem on directory dirname.
    --top - monitor the most CPU-intensive sandboxes.
    --trace - trace open, access and connect system calls.
    --tracelog - add a syslog message for every access to files or
	directories blacklisted by the security profile.
    --tree - print a tree of all sandboxed processes.
    --tunnel[=devname] - connect the sandbox to a tunnel created by
	firetunnel utility.
    --version - print program version and exit.
    --veth-name=name - use this name for the interface connected to the bridge.
    --whitelist=filename - whitelist directory or file.
    --writable-etc - /etc directory is mounted read-write.
    --writable-run-user - allow access to /run/user/$UID/systemd and
	/run/user/$UID/gnupg.
    --writable-var - /var directory is mounted read-write.
    --writable-var-log - use the real /var/log directory, not a clone.
    --x11 - enable X11 sandboxing. The software checks first if Xpra is
	installed, then it checks if Xephyr is installed. If all fails, it will
	attempt to use X11 security extension.
    --x11=none - disable access to X11 sockets.
    --x11=xephyr - enable Xephyr X11 server. The window size is 800x600.
    --x11=xorg - enable X11 security extension.
    --x11=xpra - enable Xpra X11 server.
    --x11=xvfb - enable Xvfb X11 server.
    --xephyr-screen=WIDTHxHEIGHT - set screen size for --x11=xephyr.

Examples:
    $ firejail firefox
	start Mozilla Firefox
    $ firejail --debug firefox
	debug Firefox sandbox
    $ firejail --private --dns=8.8.8.8 firefox
	start Firefox with a new, empty home directory, and a well-known DNS
	server setting.
    $ firejail --net=eth0 firefox
	start Firefox in a new network namespace
    $ firejail --x11=xorg firefox
	start Firefox and sandbox X11
    $ firejail --list
	list all running sandboxes

License GPL version 2 or later
Homepage: https://firejail.wordpress.com


