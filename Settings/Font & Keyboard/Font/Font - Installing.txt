#####################
# Font - Installing #
#####################

To install new font (ttf/otf), you can make them available for all users
via adding them to '/usr/share/fonts/' folder or by creating a folder as
'~/.fonts/' for personal use. Run 'fc-cache -f -v' afterwards to update
the system's awareness. However, the easiest way is to just use your
package manager to add more fonts.
