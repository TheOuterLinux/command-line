GitHub Page: https://github.com/yt-dlp/yt-dlp

Youtube-dl has been acting very weird and slow lately and so you may be 
better off using 'yt-dlp' instead; you can even use it to replace the
'youtube-dl' binary and it works just the same but without the slow
downloads.
