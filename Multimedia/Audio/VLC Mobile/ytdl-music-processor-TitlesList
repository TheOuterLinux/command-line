#!/bin/bash
#########################[Abstract]#############################################
# The purpose of this script is to make it easier to download audio from a     #
# youtube-dl supported URL and then have it ready for export to VLC's mobile   #
# app in order to make it easier to get away from iTunes and to help save      #
# space. This is because VLC supports the OGG format and will be using about   #
# the same amount of space as Apple's M4A format. This script reads multiple   #
# titles from a file and downloads the first YouTube result it finds. I know   #  
# this is going to sound weird, but make sure there's a blank line at the end  #
# of your songs list to make sure it downloads them all.                       #
#                                                                              #
# AFTER DOWNLOADING, IF YOU DO NEED TO STOP ADDING ID3 TAGS AND PICK BACK UP   #
# WHERE YOU LEFT OFF SOME TIME LATER, USE THE mp32ogg-batch-processor SCRIPT   #
# OR THIS SCRIPT WITHOUT ADDING "/path/to/TitlesList.txt" TO THE END.          #
#---------------------------+--------------------------------------------------------------------------#
# Requirements:             |   Use as "/path/to/ytdl-music-processor-URList"   /path/to/TitlesList.txt#
#    1. Linux/Unix system   +--------------------------------------------------------------------------#
#    2. youtube-dl          | ** It probably wouldn't hurt to place this       #
#    3. ffmpeg              |    script somewhere safe and create an alias     #
#    4. eyeD3 (pip install) |    for it.                     (o`>     /\       #
#    4. libsox-fmt-mp3      |             -- TheOuterLinux   //\     /==\      #
#    5. vorbis-tools        |                                V_/_   ======     #
################################################################################
#
#DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# ^Makes scripts portable by having $DIR/path/ relative to script location.
# I always add it just in case I need to use it.
#
while read i
do
    youtube-dl "ytsearch:$i" -ci --extract-audio --audio-format mp3 --output "%(title)s.%(ext)s" #Downloads audio and uses ffmpeg to convert to MP3
    #Using ytsearch in this way with youtube-dl causes it to accept the first search result as the download.
done < "$1"
for i in *.mp3 #For all MP3's in current directory do...
do
    type="$(file "$i" | grep -o 'layer III')"
    if [ "$type" == "layer III" ] #Just checks to make sure the file is an actual MP3 file.
    then
        fbname="$(basename "$i" .mp3)" #Removes file path and extension
        eyeD3 --remove-all "$i" 2> /dev/null #Remove all ID3 information and suppress errors.
        echo "Cleared all ID3 tags and album art for $i."
        # Otherwise, you may get an: Uncaught exception: 'NoneType' object has no attribute 'file_info'
        # This error can be ignored.
        echo ""
        echo -n "Artist: "
        read artist
        echo -n "Title: "
        read title
        echo -n "Genre: "
        read genre
        echo -n "Year: "
        read year
        eyeD3 --title="$title" --artist="$artist" --genre="$genre" --recording-date="$year" "$i" #Add ID3 tags to MP3 file.
        # You cannot add ID3 tags to OGG files with eyeD3, so you have to do it before converting.
        echo "Converting $i to OGG using Sox..."
        sox "$i" "$i.ogg" reverse silence 1 2 0.1% reverse #Convert to OGG and remove any awkwardly long silence at the end
        if [ ! -d "ytdl-music-processor-TitlesList_processed/$genre" ] #Helps keep audio files organized by genre.
        then
            mkdir -p "ytdl-music-processor-TitlesList_processed/$genre"
        fi
        mv "$fbname.mp3.ogg" "ytdl-music-processor-TitlesList_processed/$genre/$fbname.ogg" #Fix file extension
        rm "$fbname.mp3" #Delete MP3 version of audio file
        echo "Done. Processed file $fbname moved to folder ytdl-music-processor-TitlesList_processed/$genre/."
    else
        echo "$i is not an actual MP3 file. Skipping."
    fi
done
echo ""
echo "All done :)"
echo ""
#
#########################[VLC upload instructions]#############################
# After running this script, you will be left with a bunch of OGG files to be #
# brought over to your mobile device:                                         #
#     1. Make sure both devices are on the same router                        #
#     2. Open JS-enabled web browser on desktop/laptop                        #
#     3. Open VLC mobile app                                                  #
#     4. Tap the VLC cone to open the mobile app menu                         #
#     5. Select "Sharing via WiFi"                                            #
#     6. Copy the IP address into your desktop/laptop web browser             #
#     7. If it connects successfully, you should be able to just              #
#        drag-and-drop files to transfer to the VLC mobile app.               #
###############################################################################
