asciinema(1)                General Commands Manual               asciinema(1)

NAME
       asciinema - terminal session recorder

SYNOPSIS
       asciinema [-h] [--version] command [<args>]

DESCRIPTION
       Terminal  session recorder and the best companion of asciinema.org ser‐
       vice.

       asciinema is composed of multiple commands, similar to git, apt-get  or
       brew.

       When  you  run  asciinema with no arguments help messages is displayed,
       listing all available commands with their options.

OPTIONS
       -h, --help
              Display help message

       --version
              Display version information

COMMANDS
       rec [<filename>]
           Record terminal session.

           This is the single most important command in asciinema, since it is
           how you utilize this tool's main job.

           By  running asciinema rec [filename] you start a new recording ses‐
           sion. The command (process) that is recorded can be specified  with
           -c  option  (see  below),  and defaults to $SHELL which is what you
           want in most cases.

           Recording finishes when you exit the  shell  (hit  Ctrl+D  or  type
           exit).  If  the recorded process is not a shell than recording fin‐
           ishes when the process exits.

           If the filename argument is given then the resulting  recording  is
           saved to a local file. It can later be replayed with asciinema play
           <filename> and/or uploaded to asciinema.org with  asciinema  upload
           <filename>.  If the filename argument is omitted then (after asking
           for confirmation) the resulting  asciicast  is  uploaded  to  asci‐
           inema.org for further playback in a web browser.

           ASCIINEMA_REC=1 is added to recorded process environment variables.
           This can be used by your shell's config file (.bashrc,  .zshrc)  to
           alter the prompt or play a sound when shell is being recorded.

           Available options:

               -c, --command
                      specify command to record, defaults to $SHELL

               -t     specify the title of the asciicast

               -w, --max-wait
                      reduce recorded terminal inactivity to max <sec> seconds

               -y, --yes
                      answer "yes" to all prompts (e.g. upload confirmation)

               -q, --quiet
                      be quiet, suppress all notices/warnings (implies -y)

       play <filename>
           Replay recorded asciicast in a terminal.

           This  command  replays given asciicast (as recorded by rec command)
           directly in your terminal.

           When "-" is passed as a filename the asciicast is read from stdin.

           NOTE: it is recommended to run it in a terminal of  dimensions  not
           smaller than the one used for recording as there's no "transcoding"
           of control sequences for new terminal size.

           Available options:

               -w, --max-wait
                      reduce replayed terminal inactivity to max sec seconds

       upload <filename>
           Upload recorded asciicast to asciinema.org site.

           This command uploads given asciicast (as recorded by  rec  command)
           to asciinema.org for further playback in a web browser.

           asciinema  rec  demo.json  +  asciinema  play demo.json + asciinema
           upload demo.json is a nice combo for when you  want  to  review  an
           asciicast before publishing it on asciinema.org.

       auth
           Assign local API token to asciinema.org account.

           On  every  machine  you  install asciinema recorder, you get a new,
           unique API token. This command connects this local token with  your
           asciinema.org  account,  and  links all asciicasts recorded on this
           machine with the account.

           This command displays the URL you should open in your web  browser.
           If  you  never logged in to asciinema.org then your account will be
           created when opening the URL.

           NOTE: it is necessary to do this if you want to edit or delete your
           recordings on asciinema.org.

           You  can  synchronize  your config file (which keeps the API token)
           across the machines but that's not necessary. You  can  assign  new
           tokens to your account from as many machines as you want.

CONTRIBUTING
       If  you want to contribute to this project check out Contributing page:
       https://asciinema.org/contributing

BUGS
       All your bug reports and feature ideas are highly appreciated  as  they
       help  to  improve the quality and functionality of asciinema for every‐
       one.

       As the service is built of several parts there are separate bug  track‐
       ers:

       https://github.com/asciinema/asciinema/issues
              issues and ideas for the command line recorder

       https://github.com/asciinema/asciinema.org/issues
              issues and ideas for the website

       https://github.com/asciinema/asciinema-player/issues
              issues and ideas for the javascript player

AUTHORS
       Developed  with passion by Marcin Kulik and great open source contribu‐
       tors.

asciinema 1.3.0                  July 13, 2016                    asciinema(1)
