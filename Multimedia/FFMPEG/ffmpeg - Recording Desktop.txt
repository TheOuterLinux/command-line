##############################
# ffmpeg - Recording Desktop #
##############################

Record 1920x1080 x86_64 desktop in FLV format:
---------------------------------------------

    ffmpeg -f x11grab -video_size 1920x1080 -i $DISPLAY \
    -f pulse -thread_queue_size 1024 -ac 2 -i default \
    -pix_fmt yuv420p -c:v libx264 \
    -c:a aac -b:a 96k -af aresample=resampler=soxr -ar 44100 \
    -preset medium \
    -f flv \
    -threads 0 recording.flv


Record 1920x1080 i686 or Raspberry Pi desktop in FLV format:
-----------------------------------------------------------

    ffmpeg -vsync 0 -f x11grab -video_size 1920x1080 -i $DISPLAY \
    -ignore_loop 0 -i "/path/to/image" \
    -f pulse -thread_queue_size 1024 -ac 2 -i default \
    -filter_complex "[0]scale=1280:720,overlay=0:0" -pix_fmt yuv420p \
    -c:v libx264 -x264-params "nal-hdr=cbr" \
    -c:a aac -b:a 96k -af aresample=resampler=soxr -ar 44100 \
    -crf 23 -preset ultrafast \
    -b:v 1M -minrate 1M -maxrate 1M -bufsize 2M -f flv \
    -threads 0 recording.flv

    *Change 'pulse' to 'alsa' if using Raspberry Pi
    
    
Recording 1920x1080 desktop with webcam in bottom corner:
--------------------------------------------------------
    
    ffmpeg -f x11grab -video_size 1920x1080 -i $DISPLAY+0,0 -f v4l2 -i /dev/video0 \
    -f pulse -thread_queue_size 1024 -ac 2 -i default -filter_complex \
    '[1:v]colorkey=0x808080:0.01:0.01[ckout];\
    [0:v][ckout]overlay=main_w-overlay_w-2:main_h-overlay_h-2[out]' \
    -map '[out]' -pix_fmt yuv420p -c:v libx264 \
    -c:a aac -b:a 96k -af aresample=resampler=soxr -ar 44100 \
    -preset ultrafast -f flv -threads 0 recording.flv
    
    *If you know your ffmpeg, you may have noticed that I used a filter
     complex and chroma key instead of the customary way of doing this.
     This is because for some reason, this method gives me higher FPS.
     Of course, if you have a green screen and would like to use the actual
     chroma key effect, adjust 'colorkey=0x808080:0.01:0.01' as needed.
     The first value is the color to use, the second is smiliarity, and 
     the third value is blend.

Some of the scripts may appear redundant, but adding the extra doesn't seem
to hurt the frame rate of either i686 (32-bit) or Raspberry Pi. However,
the RPi will have very low FPS (~3-4) on 1080p screens. Scaling it down
seems to help for some reason. The '-thread_queue_size' is only there
because without it, I get errors on my i686 computer. The same also occurs
without '-vsync 0.' It seems as though x86_64 computers don't need vsync.
