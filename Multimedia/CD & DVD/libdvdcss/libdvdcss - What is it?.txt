###########################
# libdvdcss - What is it? #
###########################

"libdvdcss is a simple library designed for accessing DVDs like a block 
device without having to bother about the decryption." -- videolan.org 

This is a library for decrypting the CSS (Content Scramble System) in
a DVD that is there to keep people from burning copies. Because of the 
free and open source philosophy of most Linux/UNIX distributions, the
makers generally do not allow or encourage the creation of proprietary 
software. This makes playing encrypted/copy protected DVD's almost 
impossible to play without compromise. But thankfully, VideoLAN, the same
people that make VLC, came up with a solution and released it as free and
open source. Many players support it and is cross-platform.

Unfortunately, many Linux distributions do not include it by default out
of fear of DCMA-styled backlash. You will have to find a package or
compile from source (recommended).
