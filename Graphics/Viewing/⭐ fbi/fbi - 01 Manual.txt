FBI(1)                            Linux framebuffer imageviewer                            FBI(1)

NAME
       fbi - Linux framebuffer imageviewer

SYNOPSIS
       fbi [options] file ...

DESCRIPTION
       Fbi displays the specified file(s) on the linux console using the framebuffer device. Pho‐
       toCD, jpeg, ppm, gif, tiff, xpm, xwd, bmp, png and webp formats  are  supported  natively.
       For other fbi tries to use ImageMagick(1)´s convert(1).

OPTIONS
       -h, --help
              Print usage info.

       -V, --version
              Print fbi version number.

       --store
              Write command line arguments to config file ~/.fbirc.

       -l file, --list file
              Read image filelist from file.

       -P, --text
              Enable  textreading mode. In this mode fbi will display large images without verti‐
              cal offset (default is to center the images). The SPACE command will first  try  to
              scroll  down  and  go  to the next image only if it is already on the bottom of the
              page. Useful if the images you are watching are text pages, all you have to  do  to
              get the next piece of text is to press space...

       -a, --autozoom
              Enable autozoom.  Fbi will automagically pick a reasonable zoom factor when loading
              a new image.

       --(no)autoup
              Like autozoom, but scale up only.

       --(no)autodown
              Like autozoom, but scale down only.

       --(no)fitwidth
              Use width only for autoscaling.

       -v, --(no)verbose
              Be verbose: enable status line on the bottom of the screen (enabled by default).

       -u, --(no)random
              Randomize the order of the filenames.

       --(no)comments
              Display comment tags (if present) instead of the filename. Probably only useful  if
              you  added  reasonable comments yourself (using wrjpgcom(1) for example), otherwise
              you likely just find texts pointing to the software which created the image.

       -e, --(no)edit
              Enable editing commands.

       --(no)backup
              Create backup files (when editing images).

       -p, --(no)preserve
              Preserve timestamps (when editing images).

       --(no)readahead
              Read ahead images into cache.

       --cachemem size
              Image cache size in megabytes (default is 256).

       --blend time
              Image blend time in miliseconds.

       -T n, --vt n
              Start on virtual console n.

       -s steps, --scroll steps
              Set scroll steps in pixels (default is 50).

       -t sec, --timeout sec
              Start a continuous slideshow where each image is loaded  at  sec  second  intervals
              without any keypress.

       -1, --(no)once
              Don't loop (only use with -t).

       -r n, --resolution n
              Select resolution, n = 1..5 (default is 3, only PhotoCD).

       -g n, --gamma n
              Gamma  correction.  Requires  Pseudocolor  or  Directcolor visual, doesn't work for
              Truecolor (default is 1).

       -f <arg>, --font <arg>
              Set font. This <arg> can be anything fontconfig accepts (see  fonts-conf(5)).   Try
              fc-list(1)  for a list of known fonts on your system. The fontconfig config file is
              evaluated as well, so any generic stuff defined there (such  as  mono,  sans)  will
              work  as well. It is recommended to use monospaced fonts, the textboxes (help text,
              exif info) look better then.

       -d /dev/fbN, --device /dev/fbN
              Use /dev/fbN device framebuffer. Default is the one your virtual console is  mapped
              to.

       -m videomode, --mode videomode
              Name of the video mode to use (video mode must be listed in /etc/fb.modes). Default
              is not to change the video mode.

ENVIRONMENT
       Fbi uses the following environment variables:

       FBGAMMA
              This variable may be used to specify a default gamma correction.

COMMAND USAGE
       The commands take effect immediately; it is not necessary to type a carriage return.

       In the following commands, i is a numerical argument.

   Scrolling
       LEFT_ARROW, RIGHT_ARROW, UP_ARROW, DOWN_ARROW
              Scroll large images.

       PREV_SCREEN, k
              Previous image.

       NEXT_SCREEN, SPACE, j
              Next image.

       ig     Jump to image #i.

       RETURN Write the filename of the current image to stdout(3), then go to the next image.

       The RETURN vs. SPACE key thing can be used to create  a  file  list  while  reviewing  the
       images and use the list for batch processing later on:

           fbi file1.gif file2.jpg file3.jpg > fileimagelist.lst

           some RETURN and SPACE...

           fbi -l fileimagelist.lst

   Zoom
       a      Autozoom.

       +      In.

       -      Out.

       is     Set zoom to i%.

   Other
       ESQ, q Quit.

       v      Toggle status line.

       h      Display textbox with brief help.

       i      Display textbox with some EXIF info.

       p      Pause the slideshow (if started with -t, toggle).

   Edit mode
       Fbi also provides some very basic image editing facilities. You have to start fbi with the
       -e switch to use them.

       D, Shift+d
              Delete image.

       r      Rotate 90 degrees clockwise.

       l      Rotate 90 degrees counter-clock wise.

       x      Mirror image vertically (top / bottom).

       y      Mirror image horizontally (left to right).

       The delete function actually wants a capital letter D, thus you have to type Shift+d. This
       is  done to avoid deleting images by mistake because there are no safety bells: If you ask
       fbi to delete the image, it will be deleted without questions asked.

       The rotate function actually works for JPEG images only. It does a lossless transformation
       of the image.

BUGS
       Fbi  needs  rw access to the framebuffer devices (/dev/fbN), i.e you (our your admin) have
       to make sure fbi can open the devices in rw mode. The IMHO most  elegant  way  is  to  use
       PAM(7) to chown the devices to the user logged in on the console. Another way is to create
       some group, chown the special files to that group and put the users which are  allowed  to
       use  the  framebuffer  device  into  the  group. You can also make the special files world
       writable, but be aware of the security implications this has. On a private box it might be
       fine to handle it this way though.

       Fbi  also  needs access to the linux console (/dev/ttyN) for sane console switch handling.
       That is obviously no problem for console logins, but any kind of a pseudo tty (xterm, ssh,
       screen, ...) will not work.

SEE ALSO
       convert(1), fbset(1), fc-list(1), imagemagick(1), wrjpgcom(1), fonts-conf(5), PAM(7)

AUTHOR
       Gerd Hoffmann <gerd@kraxel.org>

COPYRIGHT
       Copyright (c) 1999-2012 Gerd Hoffmann <gerd@kraxel.org>

       This program is free software; you can redistribute it and/or modify it under the terms of
       the GNU General Public License as published by the Free Software Foundation;  either  ver‐
       sion 2 of the License, or (at your option) any later version.

       This  program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
       without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR  PURPOSE.
       See the GNU General Public License for more details.

       You should have received a copy of the GNU General Public License along with this program;
       if not, write to the Free Software Foundation, Inc., 675 Mass Ave,  Cambridge,  MA  02139,
       USA.

FBI 2.09                           (c) 1998-2012 Gerd Hoffmann                             FBI(1)
