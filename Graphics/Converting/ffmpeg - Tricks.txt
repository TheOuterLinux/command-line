###############################
# ffmpeg - Better quality GIF #
###############################

When you use 'ffmpeg -i input.mp4 output.gif', the animated GIF is very
grainy looking. You can use the following to get better quality without
increasing the file size by too much...

1. Get a color palette from the video:

    ffmpeg -i <your_input.mkv> -filter_complex "fps=10;scale=480:-1:flags=lanczos,palettegen=stats_mode=full" palette.png
    
2. Convert video to animated GIF using color palette:

    ffmpeg -i <your_input.mkv> -i palette.png -filter_complex "[0]scale=480:-1:flags=lanczos[scaled]; [scaled][1:v] paletteuse=dither=sierra2_4a" <output.gif>

###################################################### 
# ffmpeg - Getting a 256 color palette from an image #
######################################################

ffmpeg -i file.png' -vf scale=320:-1:flags=lanczos,format=rgb8,palettegen palette.png

############################################################################
# ffmpeg - Converting an mp4 to anmited GIF using a specific color palette #
############################################################################

ffmpeg -v warning -i test.mp4 -i /tmp/palette.png -lavfi paletteuse -y test.gif

##########################################################
# ffmpeg - Decrease the file-size of large animated GIFs #
##########################################################

The following doesn't always work, but:

ffmpeg -i input.gif -filter_complex "[0:v] split [a][b];[a] palettegen [p];[b][p] paletteuse" -output.gif
