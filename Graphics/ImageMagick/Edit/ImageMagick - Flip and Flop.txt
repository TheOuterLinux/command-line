###############################
# ImageMagick - Flip and Flop #
###############################

Flip = ^v
Flop = <->

Flipping an image:

    mogrify -flip "/path/to/image"
    
Flopping an image:

    mogrify -flop "$/path/to/image"
