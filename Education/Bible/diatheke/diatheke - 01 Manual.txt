DIATHEKE(1)                             Diatheke man page                             DIATHEKE(1)

NAME
       diatheke - a command line Bible reader

SYNOPSIS
       diatheke  -b  module_name  [-o  option_filters] [-m maximum_verses] [-f output_format] [-e
       output_encoding] [-t script] [-v variant_number] [-l locale] -k query_key

       diatheke -b  module_name  -s  regex|multiword|phrase  [-r  search_range]  [-l  locale]  -k
       search_string

       diatheke -b system -k modulelist|modulelistnames|localelist

       diatheke -b info -k module_name

DESCRIPTION
       diatheke prints Bible verses or other text from the modules which the Sword library uses.

OPTIONS
       -b     Module  name.  May  be  "system"  or  "info"  (see  QUERY KEYS) or one of the names
              obtained by using "diatheke -b system -k modulelist".

       -s     Search type. One of regex (regular expression, see regex(7)), multiword (like "word
              [AND word]..."), phrase (exact text).

       -r     Search  range. A valid Bible key range value (see -k). For example: Matt-John, Rom,
              gen-psalms, 1Thess 1:5-2:6.

       -o     Module option filters. A combination of "fmhcvalsrbx". See MODULE OPTIONS.

       -m     Maximum number of verses returned. Any integer value.

       -f     Output format. One of GBF, ThML, RTF, HTML, OSIS, CGI, plain (default).  This  cur‐
              rently  works  only  if there is a Sword library filter from the original format to
              the specified output format. HTML and CGI are for generating text for a webpage.

       -e     Output character encoding. One of Latin1, UTF16, HTML, RTF, UTF8  (default).   This
              currently works only for module contents, not for system key contents or key texts.

       -t     Script.  (This  seems  to  be  broken  in  version  4.2.1,  it  should  allow  e.g.
              Greek->Latin1 character transliteration with "-t Latin".)

       -v     Variant. The text may contain variant readings. One of -1 (all), 0, 1.

       -l     Locale. Sword may have different locales installed.  Default  is  en.  Localization
              affects input and output keys.

       -k     Query  key.  It must be the last argument because all following arguments are added
              to the key. See QUERY KEYS for different kinds of keys.

MODULE OPTIONS
       By default the optional features of modules are not shown  in  the  returned  text.  These
       options make them visible if the module supports them.

       n      Strong's  numbers.  These numbers refer to the Strong's dictionaries of the New and
              Old Testaments. Some Bible texts attach these numbers to words.

       f      Footnotes.

       m      Morphology of the Greek/Hebrew words. Morphology is shown as a code which refers to
              an entry in some dictionary-type module.

       h      Section headings.

       c      Hebrew cantillation.

       v      Hebrew vowels.

       a      Greek accents.

       l      Lemmas (the base forms of the words).

       s      Scripture cross-references.

       r      Arabic shaping.

       b      Bi-directional reordering.

       x      Red Words of Christ.

QUERY KEYS
       Bible texts and Commentaries
              use  verse  keys. Examples: john 1:1, j1:1 jh1 (the first chapter of John), jh (the
              whole book of Gospel of John), joh 1:1-3 ( a verse range), joh 1:0 (one verse back‐
              wards  from 1:1, which is the last verse of the previous book or possibly an intro‐
              duction to John), joh 1:100 (which is 100 verses forward from the  first  verse  of
              John 1:1), 1234 (which is the 1234th verse from the beginning of the Bible).

       Lexicons and Dictionaries
              use  word  keys.  Any  word can be used and similar or next entry alphabetically is
              returned.  Some dictionaries like Strong's use numbered entries.

       system is not a module but when used with -b allows one of these keys: modulelist (list of
              the  available modules with short descriptions), modulelistnames (list of the names
              of the available modules), localelist (list of the available Sword locales).

       info   is not a module but when used with -b allows a module name as a key and gives  some
              information about that module.

EXAMPLES
       diatheke -b KJV -k joh1:1
              Shows John 1:1 in King James Version.

       diatheke -b RWP -k Matthew 2:2
              Shows Matthew 2:2 in Robertson's Word Pictures commentary.

       diatheke -b WebstersDict -k bible
              Show entry "bible" in Websters Dictionary.

       diatheke -b system -k modulelist
              Shows the list of available modules.

       diatheke -b KJV -o fmslx -f OSIS -e Latin1 -k john 1:1-3
              Shows  John 1:1-3 from KJV in OSIS XML format in iso8859-1 encoding with footnotes,
              morphology, cross-references, lemmas and words of Christ in red.  (The  module  may
              not  support  all options and those have no effect. KJV includes only ASCII charac‐
              ters so encoding has no effect.)

       diatheke -b GerLut -l de -m 10 -k Offenbarung
              Shows the first 10 verses of Revelation in German GerLut version,  both  input  and
              output keys are localized.

       diatheke -b KJV -s phrase -r Mt -k love
              Shows those verse keys which include phrase "love" in Gospel of Matthew in KJV mod‐
              ule.

DIAGNOSTICS
       Currently diatheke exits always with status 0. If only Bible verse key and module name are
       shown it means that the module did not have any content in that range. If only module name
       is shown the key may have been bad. If nothing is shown the book name may have  been  bad.
       If Segmentation fault is shown diatheke has been bad.

SEE ALSO
       http://www.crosswire.org/sword/

COPYRIGHT
       Sword library: © 1994-2006 Crosswire Bible Society, released under GPL licence.  Diatheke:
       written by Chris Little, © 1999-2006 Crosswire Bible Society, released under GPL  licence.
       This manpage was written by Eeli Kaikkonen and may be used freely.

diatheke 4.2.1                              2009-02-01                                DIATHEKE(1)
