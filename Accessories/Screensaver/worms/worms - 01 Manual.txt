WORMS(6)                                 BSD Games Manual                                WORMS(6)

NAME
     worms — animate worms on a display terminal

SYNOPSIS
     worms [-ft] [-d delay] [-l length] [-n number]

DESCRIPTION
     A UNIX version of the DEC-2136 program “worms”.

     The options are as follows:

     -f      Makes a “field” for the worm(s) to eat.

     -t      Makes each worm leave a trail behind it.

     -d      Specifies a delay, in milliseconds, between each update.  This is useful for fast
             terminals.  Reasonable values are around 20-200.  The default is 0.

     -l      Specifies a length for each worm; the default is 16.

     -n      Specifies the number of worms; the default is 3.

                                           May 31, 1993
