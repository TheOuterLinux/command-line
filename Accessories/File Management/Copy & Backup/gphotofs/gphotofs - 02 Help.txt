Usage:
  gphotofs [OPTION...] - gphoto filesystem

Help Options:
  -?, --help          Show help options

Application Options:
  --port=path         Specify port device
  --speed=speed       Specify serial transfer speed
  --camera=model      Specify camera model
  --usbid=usbid       (expert only) Override USB IDs
  -h, --help-fuse     Show FUSE help options

