GITWIPE(1)                           General Commands Manual                           GITWIPE(1)

NAME
       gitwipe - An utility for wiping files

SYNOPSIS
       gitwipe file ...

DESCRIPTION
       This manual page documents briefly the gitwipe command.

       gitwipe  is  an  utility  for wiping files.  It overwrites the file contents with a random
       sequence of numbers and then calls `sync'().

       Note that gitwipe does not remove the wiped  file  since  (under  `Linux'  at  least)  the
       `sync'()  system  call might return before actually writing the new file contents to disk.
       Removing the file might be dangerous because some file systems can detect that the  blocks
       in the removed wiped file are no longer used and never write them back to disk in order to
       improve performance.  It is up to you to remove the file(s) at a later moment.

       gitwipe is part of the GNU Interactive Tools.

SEE ALSO
       gitfm(1)

       gitwipe and gitfm are documented fully by GNU Interactive Tools, available  via  the  Info
       system.

BUGS
       Please send bug reports to:
       gnuit-dev@gnu.org

AUTHORS
       Tudor Hulubei <tudor@cs.unh.edu>
       Andrei Pitis <pink@pub.ro>
       Ian Beckwith <ianb@erislabs.net> (Current maintainer)

                                           Sep 30, 2007                                GITWIPE(1)
