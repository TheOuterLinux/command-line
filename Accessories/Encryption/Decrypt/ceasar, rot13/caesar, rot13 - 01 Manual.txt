CAESAR(6)                                BSD Games Manual                               CAESAR(6)

NAME
     caesar, rot13 — decrypt caesar ciphers

SYNOPSIS
     caesar [rotation]

DESCRIPTION
     The caesar utility attempts to decrypt caesar ciphers using English letter frequency statis‐
     tics.  caesar reads from the standard input and writes to the standard output.

     The optional numerical argument rotation may be used to specify a specific rotation value.

     The frequency (from most common to least) of English letters is as follows:

           ETAONRISHDLFCMUGPYWBVKXJQZ

     Their frequencies as a percentage are as follows:

           E(13), T(10.5), A(8.1),  O(7.9),  N(7.1),  R(6.8),  I(6.3),  S(6.1),  H(5.2),  D(3.8),
           L(3.4),  F(2.9),  C(2.7), M(2.5), U(2.4), G(2), P(1.9), Y(1.9), W(1.5), B(1.4), V(.9),
           K(.4), X(.15), J(.13), Q(.11), Z(.07).

     Rotated postings to USENET and some of the databases used by the fortune(6) program are
     rotated by 13 characters.

BSD                                     November 16, 1993                                     BSD
