###############
# sed - Basic #
###############

Remove trailing whitespace from a file:

    sed -i 's/[[:space:]]*$//' file.java
    
Removing trailing whitespace from multiple files:

    find . -name "*.java" -type f -print0 | xargs -0 sed -i 's/[[:space:]]*$//'

Removing empty lines

    sed -i '/^$/d' file.txt
