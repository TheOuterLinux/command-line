######################
# python - Functions #
######################

Here are some examples of functions with python. Please note that you
will probably have to install the packages 'python3-sympy' for these
to work since it doesn't come by default in most Linux/UNIX systems.
If you package manager doesn't have it, you can try using pip:

    sudo pip install sympy

1. A ghoul was trotting down the sidewalk and noticed a large spiderweb. 
   Being a math wizard as most ghouls are, noticed 4 tasty moths trapped 
   in one quadrant, 3 juicy beetles in another quadrant, 2 sour spiders 
   in another, and 1 confused worm in the last. If this ghoulish buffet 
   had been three times better, how many of each snack would there be?

	>>>from sympy import *
 	>>>Matrix([[4,3],[2,1]])*3

2. As punishment for not scaring enough trick-or-treaters, Casper the 
   ghost was told to factor this dreadful equation, 24x^2*y+34xy+12y. 
   How could he do this with Python?

	>>>from sympy import *
	>>>x = Symbol('x')
	>>>y = Symbol('y')
	>>>factor(24*x**2*y + 34*x*y + 12*y)

3. Casper the ghost was then told to take this factored equation, 
   3(2x+y)(4x-3y), and expand it. How could he do this with Python?

	>>>from sympy import *
	>>>x = Symbol('x')
	>>>y = Symbol('y')
	>>>expand(3*(2*x+y)*(4*x-3*y))

4. Satan is building a fence for Hell. Hell is an infinitely (oo) large 
   and weird place. So if a formula was to represent the amount of fencing 
   needed, he determined it to be y=x^x. However, Satan has a budget and 
   needs to show mathematically to God as to how ridiculous that is. 
   Using Python, what can he do?

	>>>from sympy import *
	>>>x = Symbol('x')
	>>>limit(x**x, x, oo) #The limit of x as it gets close to infinity (oo)

5. At the start of Halloween, a hungry wicked witch smells a lost child 
   a mile away. She takes off from the ground at 100 MPH (44.704 m/s) 
   and then lets gravity do the rest once she reaches as high as she can 
   go. If gravity is 9.8 m/s, at what point in time will she reach her 
   maximum height?

	>>>from sympy import *
	>>>t = Symbol('t')
	>>>height = 44.704*t - 9.8*t**2
	>>>yprime = height.diff(t) #To find the derivative of 'height'
	>>>solve(yprime, t)

6. Use integrate() for integrals.

	>>>integrate(yprime)
	
You can find the rest of the Halloween themed math video here:
https://www.bitchute.com/video/zbgQc6eoodO9

>>>solve([x + 5*y -2, 6*y -15],[x,y])
>>>{y:1, x:-3}

>>>solve(x**4 -1, x)
>>>[I, 1, -1, -I] #Because there are two points in which x touches
