################
# when - Basic #
################

Edit calendar file:
------------------

    when e
    
Event examples:
--------------
    
    * dec 25, Christmas --> Event that occurs once a year
    
    1900* jan 1 , John Doe turns \a, born in \y --> Shows age and birthday
    
    w=sun , go to church, 10:00 --> Weekly events
    
    2018 jul 4 , 8:00p fireworks show
    
    d=1 | d=15 , Pay employees --> Reminder on 1st and 15th day of every month
    
    w=sat & b=1 , Rehearse with band --> last Saturday of every month
    
    w=fri & !(m=dec & d=25) , poker game --> Every Friday except Christmas
    

Show events within next two weeks:
---------------------------------

    when
    
Show events within only a single...
--------------------------------

    Week: when w
    Month: when m
    Year: when y
    
Show current and two months ahead in grid format:
------------------------------------------------

    when c
    
There's a preference file for changing default displayed information here:

    ~./when/preferences
    
    
