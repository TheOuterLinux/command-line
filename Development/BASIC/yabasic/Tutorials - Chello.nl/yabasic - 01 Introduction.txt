From: http://members.chello.nl/~a.vanarum8/Yabasic/bgintroduction.htm

Beginner's Guide: 
" Introduction to Programming"

For many, the concept of programming refers to some kind of mystic experience
that belongs to the exclusive domain of meditating gurus, also known as
software engineers or programmers. However, believe it or not, programmers are
not higher beings from another planet, but, just like politicians and
pop-icons, humans like you and me. And it is good to keep in mind that, just
like some of today’s pop-icons, they are often even fairly ordinary human
beings, with no more special gifts than a little dexterity and a little
patience.

And that is all it takes to learn how to program. Trust me when I tell you that
the English language is infinitely more complex than programming usually is
(except when you try to learn a computer the English language of course). And
although it is true that most of us spent a considerable time of our lives
mumbling and gaga-ing before we could utter a decent sentence, the good news is
that programming only implements a tiny fraction of the knowledge and skill
that English requires, and even then there is a considerable amount of
overlap. 

The overlap between the English language and programming lies mainly in the
area of telling people what to do. And if you’re anything like me, this should
tell you that this means programming can be a lot of fun. If you were a very
obedient person, and I would tell you to stand on one hand and then touch your
nose with your free hand, you’d do it. Unless you were a pretty athletic and
acrobatic type, you’d probably fail horribly and get hurt in the process, but
you’d do it, and so would a computer (and unless its that wonderful new Sony
robot-dude, including the failing horribly part). 

However, there are two important factors that make telling a computer what to
do different from telling a computer what to do. First of all, computers aren’t
very clever. Unless you give it detailed instructions, it will not know what
standing on one hand is, what a nose is, that you normally stand on two feet,
and that pressing one hand against the floor while bending over isn’t exactly
what you meant when you told it to stand on one hand. 

Second of all, we will, for now at least, be programming on computers which
have a slightly different anatomy from human beings. A computer doesn’t have
senses, limbs and organs, but it has input and output devices, like a screen, a
soundcard (both output devices), and a keyboard, mouse and joy-pads (input
devices). In a sense, if you were comparing a standard computer and a human
being from the point of view of the human being, a computer would be a lot like
a very dumb but obedient person trapped inside a glass box. The only means of
communication with this person is through one of the input devices, in a
language that is a combination of a very limited form of English and a bunch of
electronic impulses. Programming would then be something like this: you would
tell this ‘computer’ to write “Hello” in reverse on the inside of the window by
typing the instruction to do so on the keyboard:

  write ‘Hello’ in reverse on the window

… and that is what it would do. However, since it can only really draw
something on the window most of the time, you can leave that out, and since it
is always writing and drawing things for the person on the other side of the
screen, it always draws in reverse anyway, so you can leave that out too. So
the instruction becomes:

  write ‘Hello’ 

And the result would be “Hello” written on the screen. In the particular
dialect of programming that is Yabasic, 'print' is the word used for the
'write' instruction, so that the real instruction that will work in Yabasic is:

  print "Hello"

And the result in the text screen would be "Hello". 

You could also tell this ‘computer’ to “draw ‘duck’” on the screen, but it
wouldn’t understand you – I told you, ‘dumb’ – but on the other hand, if you
told him to draw two centimeters to the left, then two centimeters to the right
and one up, etc, until something like a duck appears, it would do so very
precisely. You could even tell it in 600 steps how to draw a beautiful duck,
and it would remember all those 600 steps at once and perform them one after
the other, at the speed of light (well, almost) and with no mistakes. 

Now there is something no ordinary human being can do – after I give someone
directions to the post-office, often if it involves more than 5 instructions
(go left, then the second right, then left again, then straight on past the
traffic lights, then the second street on the right), most people will go “uh,
what was that first turn again?”. 

(c) 2001 Arwin van Arum

Sony and Playstation 2 are registered trademarks owned by Sony Inc.

 

