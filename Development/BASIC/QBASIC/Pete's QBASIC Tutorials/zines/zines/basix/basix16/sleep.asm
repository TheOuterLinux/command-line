.MODEL MEDIUM,BASIC ;QuickBasic uses the Medium model
.STACK 200h         ;Define our small stack
.CODE
ASMSleep proc FAR   ;The SUB ASMSleep
Public ASMSleep     ;Make it pubblic to QB
MOV AX,00           ;Interrupt 16h service 0
INT 16h             ;Call the INTerrupt
RET                 ;Return to Basic
ENDP ASMSleep       ;End of the sub
END                 ;End of the file
