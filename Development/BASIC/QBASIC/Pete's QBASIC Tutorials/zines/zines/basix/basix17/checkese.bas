DECLARE FUNCTION Checkese$ (Amount AS STRING)
DECLARE FUNCTION CheckeseCurrency$ (Amount AS STRING)

CLS

FOR x = 1 TO 25
        READ n$
        LOCATE 2, 1: PRINT SPACE$(80)
        LOCATE 3, 1: PRINT SPACE$(80)
        LOCATE 2, 1: PRINT CheckeseCurrency$(n$) + " > "
        LOCATE 3, 1: PRINT Checkese$(n$)
        LOCATE 25, 1: PRINT "Press any key to continue..."
        SLEEP
NEXT x

END

DATA 13, 763
DATA 8033, 12245
DATA 789003, 3594006
DATA 62095239, 104592350
DATA 1110439595, 93125687090
DATA 6593584958205, 5294582000042
DATA 23846309766532, 634687239587298
DATA 462980003246345, 5246287598729879
DATA 2104820002345283, 72358729798579285
DATA 46349820994868692, 100000006246724355
DATA 210104247600030246, 6348679872200902452
DATA 6049680935200023568, 10000500000004235235
DATA 346343735389470394583048530

FUNCTION Checkese$ (Amount AS STRING)

DIM Amt         AS STRING
DIM WholeValue  AS STRING
DIM DecimValue  AS STRING

DIM Chunk       AS STRING
DIM ChunkVal    AS STRING
DIM SVal        AS STRING

DIM Value       AS INTEGER
DIM OnesValue   AS INTEGER
DIM NumGroups   AS INTEGER

DIM GroupingAppend(1 TO 6) AS STRING
        GroupingAppend(1) = ""
        GroupingAppend(2) = "thousand"
        GroupingAppend(3) = "million"
        GroupingAppend(4) = "billion"
        GroupingAppend(5) = "trillion"
        GroupingAppend(6) = "quadrillion"
       
DIM Numeral(1 TO 2, 0 TO 9) AS STRING
        Numeral(1, 0) = "": Numeral(2, 0) = "and "
        Numeral(1, 1) = "one": Numeral(2, 1) = "TEEN-"
        Numeral(1, 2) = "two": Numeral(2, 2) = "twenty"
        Numeral(1, 3) = "three": Numeral(2, 3) = "thirty"
        Numeral(1, 4) = "four": Numeral(2, 4) = "forty"
        Numeral(1, 5) = "five": Numeral(2, 5) = "fifty"
        Numeral(1, 6) = "six": Numeral(2, 6) = "sixty"
        Numeral(1, 7) = "seven": Numeral(2, 7) = "seventy"
        Numeral(1, 8) = "eight": Numeral(2, 8) = "eighty"
        Numeral(1, 9) = "nine": Numeral(2, 9) = "ninety"

DIM Teen(0 TO 9) AS STRING
        Teen(0) = "ten"
        Teen(1) = "eleven"
        Teen(2) = "twelve"
        Teen(3) = "thirteen"
        Teen(4) = "fourteen"
        Teen(5) = "fifteen"
        Teen(6) = "sixteen"
        Teen(7) = "seventeen"
        Teen(8) = "eighteen"
        Teen(9) = "nineteen"

Amt = Amount

IF LEN(Amt) > 20 THEN
        Checkese$ = "ERROR"
        EXIT FUNCTION
        END IF

DO WHILE LEFT$(Amt, 1) = "0"
        Amt = RIGHT$(Amt, LEN(Amt) - 1)
LOOP

' // Get Decimals //

Chunk = RIGHT$(Amt, 2)
DecimValue = Chunk + "/100 dollars"
Amt = LEFT$(Amt, LEN(Amt) - 2)

IF LEN(Amt) = 0 OR VAL(Amt) = 0 THEN
        Checkese$ = DecimValue
        EXIT FUNCTION
        END IF

' // Get All Other Digits //

NumGroups = 1
DO UNTIL LEN(Amt) < 1
        IF LEN(Amt) >= 3 THEN Chunk = RIGHT$(Amt, 3)
        IF LEN(Amt) <= 2 THEN Chunk = Amt

        ' // RIGHTMOST character //
        SVal = MID$(Chunk, LEN(Chunk), 1)
        Value = VAL(SVal)
        ChunkVal = Numeral(1, Value) + ChunkVal
        OnesValue = Value
        IF LEN(Chunk) = 1 THEN GOTO EndChunk
                
        ' // MIDDLE character //
        SVal = MID$(Chunk, LEN(Chunk) - 1, 1)
        Value = VAL(SVal)
        TensValue = Value
        IF OnesValue <> 0 AND Value <> 0 THEN ChunkVal = "-" + ChunkVal
        ChunkVal = Numeral(2, Value) + ChunkVal
        IF LEFT$(ChunkVal, 5) = "TEEN-" THEN ChunkVal = Teen(OnesValue)
        IF LEN(Chunk) = 2 THEN GOTO EndChunk

        ' // LEFTMOST character //
        SVal = MID$(Chunk, LEN(Chunk) - 2, 1)
        Value = VAL(SVal)
        IF Value = 0 THEN
                IF OnesValue = 0 AND TensValue = 0 THEN
  GOTO AllZero
                ELSE
  GOTO EndChunk
  END IF
                END IF
        ChunkVal = Numeral(1, Value) + " hundred " + ChunkVal
        IF OnesValue = 0 AND TensValue = 0 THEN
                ChunkVal = Numeral(1, Value) + " hundred"
                END IF

EndChunk:
        ChunkVal = ChunkVal + SPACE$(1) + GroupingAppend(NumGroups) + ","
        WholeValue = ChunkVal + SPACE$(1) + WholeValue

AllZero:
        Value = LEN(Amt)
        IF Value >= 3 THEN Amt = LEFT$(Amt, LEN(Amt) - 3)
        IF Value <= 2 THEN Amt = ""

        NumGroups = NumGroups + 1
        ChunkVal = ""
        Value = 0

LOOP

DO WHILE RIGHT$(WholeValue, 1) = SPACE$(1) OR RIGHT$(WholeValue, 1) = ","
        WholeValue = LEFT$(WholeValue, LEN(WholeValue) - 1)
LOOP

MID$(WholeValue, 1, 1) = UCASE$(LEFT$(WholeValue, 1))

Checkese$ = WholeValue + SPACE$(1) + "and" + SPACE$(1) + DecimValue

END FUNCTION

FUNCTION CheckeseCurrency$ (Amount AS STRING)

DIM Amt         AS STRING
DIM DecimValue  AS STRING
DIM WholeValue  AS STRING

DIM StringBuff  AS STRING

Amt = Amount

DO WHILE LEFT$(Amt, 1) = "0"
        Amt = RIGHT$(Amt, LEN(Amt) - 1)
LOOP

DecimValue = RIGHT$(Amt, 2)
Amt = LEFT$(Amt, LEN(Amt) - 2)

DO UNTIL LEN(Amt) < 3
        StringBuff = RIGHT$(Amt, 3)
        WholeValue = "," + StringBuff + WholeValue
        Amt = LEFT$(Amt, LEN(Amt) - 3)
LOOP

Amt = Amt + WholeValue + "." + DecimValue

DO WHILE LEFT$(Amt, 1) = "," OR LEFT$(Amt, 1) = " "
        Amt = RIGHT$(Amt, LEN(Amt) - 1)
LOOP

Amt = "$" + SPACE$(1) + Amt

CheckeseCurrency$ = Amt

END FUNCTION

