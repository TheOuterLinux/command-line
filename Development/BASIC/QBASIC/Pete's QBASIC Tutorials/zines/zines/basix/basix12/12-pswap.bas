' Screen swapping demo, by Alex Warren
' from the Basix Fanzine, Issue 12

RANDOMIZE TIMER
DEFINT A-Z
SCREEN 0: CLS : WIDTH 80

PRINT "Press any key to start the screen swapping demo..."
DO: LOOP WHILE INKEY$ = ""

SCREEN 7
PRINT "Pages are being drawn while this page is"
PRINT "being displayed..."

FOR i = 1 TO 7
        SCREEN 7, , i, 0

        FOR j = 0 TO 200 STEP i
                LINE (0, j)-(320, j), i
        NEXT j
        
        FOR j = 0 TO 320 STEP i
                LINE (j, 0)-(j, 200), i
        NEXT j

        FOR j = 1 TO i * 1000
                PSET (RND * 320, RND * 200), RND * 15
        NEXT j

        LOCATE 2, 2
        COLOR i + 1
        PRINT "SCREEN PAGE"; i
NEXT i

BEEP

SCREEN 7, , 0, 0
LOCATE 4, 1
PRINT "Press the space bar to go to the next"
PRINT "page, and press [ESC] when done"

cpage = 1

DO
FOR cpage = 1 TO 7
        DO
                a$ = INKEY$
        LOOP WHILE a$ <> CHR$(27) AND a$ <> " "

        IF a$ = CHR$(27) THEN END

        SCREEN 7, , , cpage
NEXT cpage
LOOP


