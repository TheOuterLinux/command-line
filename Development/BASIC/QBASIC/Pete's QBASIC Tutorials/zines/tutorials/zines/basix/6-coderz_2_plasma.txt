______________________________________________________________________________
| SECTION 1 PART A SUBPART 2 | Coderz Series |
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 Welcome to this new series. If you want to create impressive looking effects
 from  QBasic  possibly  for demos,  effects for games or just to learn a bit
 of  math  then keep reading,  if you want to improve your databases or learn
 some  Mode-X routines then steer clear :) This series primarily goes through
 the  theory of creating various effects such as plasma, tunnels etc and then
 presents  source  code  which shows a way these could be done in QBasic, but
 they  will  probably not be optimized but that will make a good exercise for
 you  the  reader,  you  could also convert them into Mode-X or various other
 screen modes. 

 So lets move straight onto our Plasma.
 ____________________
 - Plasma -----------
 ~~~~~~~~~~~~~~~~~~~~
 We  are going to be constructing a _sort_ of plasma. Ok, strictly it may not
 be  a plasma but it produces some plasma like patterns and with some palette
 rotations  it  can look very effective!  We  are going to use our old friend
 the  Sine  Wave.  Without  going into the theory of how sines waves are what
 they are I will explain briefly..  The  sinewave  goes in a range of -1 to 1
 going  through 0 as a middle point, a width of sine wave is 360 degrees.  We
 can  get the y position of the sine wave at any of these 360 points by using
 the SIN command. Unfortunately the SIN command requires the input in Radians
 but we can convert degrees to radians by multiplying the degrees by 0.017453

 So, to get the Y position of the sine wave at 90 degrees we would use this:

 PRINT SIN(90*0.017453)

 and it would give us an answer near to 1.

Fig 1========================================================================

 +1|              /  \
   |          /         \
   |       /               \
   |    /                      \
   |  /                            \                                      
  0|/                                 \                                  /
   |                                     \                            /
   |                                        \                      /
   |                                          \                 /
   |                                              \          /
 -1|                                                   \  /

dgs  0            90                  180               270             360

=============================================================================

 Of course, using a scale of -1 to +1 is a bit limited in my opinion and it
 requires a bit of multiplication and addition so that we can get a range of
 something like 0 to 100 with 50 being the mid point. To make this so:

 PRINT (SIN(dg%*0.017453)*50)+50

 We can actually draw a sine wave on our screens. To do this try this:

 SCREEN 13
 FOR x% = 0 to 360
 PSET (x%, ( SIN(x%*0.017453)*50 )+50),15
 NEXT x%

 But we have one major problem. A sine wave is 360 degrees wide but our mode
 13 is only 320 pixels wide. We'll get an error. So what we'll do is just
 scale down the x% into another variable called xx%

 SCREEN 13
 FOR x% = 0 to 360
 xx% = (x% /360)*320
 PSET (xx%, ( SIN(x%*0.017453)*50 )+50),15
 NEXT x%

 If  you  run  this you'll discover the sinewave goes from middle to a bottom
 then up to a top and then back down to the middle.. Then you'll see our Fig1
 and  think,  hang on,   he's got it round the wrong way.. No, we havn't it's
 just  because  on  our  chart the higher the number the higher it is, but on
 the computer screen it is the opposite, ok?

 Ok,  so  now  we have a sinewave. The sinewave has _amplitude_, this is what
 the range is, in our case it is 0 to 100. If the range was 0 to 50 it'd have
 a  lower  amplitude.  Our  sinewave  also has _frequency_, this is basically
 how  many  sinewaves  we can fit in a certain amount of space, if instead of
 there  being only one wave across the screen there was two then we could say
 that has more frequency, or a _higher_ frequency.

 We  can  also  _mix_  sinewaves.  For example we can mix sinewaves that have
 different  frequencies.  To  be  able  to add this lets simplify our program
 so far:

 SCREEN 13
 FOR x% = 0 to 360
 xx% = (x% /360)*320
 rd = (x% * 0.017453)
 srd = SIN(rd)
 PSET (xx%, ( srd*50 )+50),15
 NEXT x%

 This  program will work just as normal. Now, we want to _mix_ two sinewaves,
 how  can  we do this? Ok, what we can do is take our normal sinewave and mix
 it  with one with a higher frequency! We can do his this by taking the line:

 srd = SIN(rd)

 which  only  creates  a  single wave and "add" another wave with a different
 frequency into it and then average out the pair, like so:

 srd = (SIN(rd)+SIN(rd*1.8))/2

 If  you  make  that  adjustment  and run the program you'll get a wave which
 looks more  random  and out of control and this is what makes good patterns.
 Of  course  you  can  mix  three sinewaves, just add in the relevant SIN and
 change  the  2 to 3.  Simple.  You  could also speed up the program by using
 lookup  tables  or  other tricks. But this is not about optimization, we are
 concerned with theory and getting it to work.

 So  what  has this got to do with colorful patterns and plasma then? you may
 be  asking.  Well  the  sinewaves  give  us  something to create interesting
 palette  setups with.  To be able to demonstrate this better lets adjust the
 program  so  far  so  that  it  goes in a range of 0 to 63 which is what the
 palette uses.

 SCREEN 13
 FOR x% = 0 to 360
 xx% = (x% /360)*320
 rd = (x% * 0.017453)
 srd = (SIN(rd)+SIN(rd*2.8))/2
 PSET (xx%, ( srd*31 )+31),15
 NEXT x%

 Notice  I've  changed  the 1.8 to a 2.8 in the 2nd sinewave, this just makes
 the  wave more "wild", now we have a sinewave that can be used on a palette.
 Lets have a go.  Lets use this sinewave to adjust the palette in colors 1 to
 100. 

 SCREEN 13
 FOR x% = 0 to 360
 xx% = (x% /360)*100    ' notice the change to 100 to scale to 100
 rd = (x% * 0.017453)
 srd = (SIN(rd)+SIN(rd*2.8))/2
 OUT &H3C8,xx%+1
 OUT &H3C9,(srd * 31)+31
 OUT &H3C9,0
 OUT &H3C9,0
 NEXT x%

 Now  this  creates  a fine palette, lets see it in action! Add the following
 code to the end:

 FOR x% = 1 TO 100
 FOR y% = 1 TO 100
 PSET (x%, y%), (x%+y%)/2
 NEXT y%
 NEXT x%

 Now we have a great little effect.

 In Closing
 ~~~~~~~~~~
 Next  Issue  we'll  finish  off  with this,  it's a very wide topic. You can
 adjust  our program so far for different color types, fade from red to blue,
 For example: if you replace the first OUT &H3C9,0 with 
                        OUT &H3C9,63-((srd * 31)+31)
 you'll  get  a  red  and  green  fading  mixture.  Feel  free to send me any
 improvements  and feel free to use the code, it's all theory that anyone can
 work  out  so  feel free to take the code and dream up some things with it..
 You should see it with palette rotations, it looks great. I'll also show you
 how  to create FAR better effects using this sine wave method, but using TWO
 completely  separate  sinewave constructions, one for X axis and one for Y..
 See if you can do it.

 Maybe I'll include some better programs with it next month.

 Next month:  Finishing sines, Tunnels








--------------------------------------------------------

*	EDITOR'S NOTE:
*	This article was originally printed in Peter Cooper's BASIX Fanzine, 
*	Issue #6 from August 1996.