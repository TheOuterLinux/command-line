______________________________________________________________________________
| SECTION 1 PART A SUBPART 1 | GFX Colliding! |
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 Our regular article from Wrox Press!
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 Perhaps I had better go into a more detailed description of the book. If 
 you ever want to see more of the companies books and samples of them then
 check out their WWW site:   point your URL browser at:  http://www.wrox.com/
 See more info in the Q+A section of the fanzine about what is in the book.

COLLISION DETECTION
~~~~~~~~~~~~~~~~~~~
We could convert the previous example to check for our sprite crashing into 
the  furniture,  but  it would be slow and tedious if the room were full of 
armchairs. 

So we must use another method for detecting collisions between sprites. One 
way  is  to  check  certain pixels around our sprite for a change in color, 
using  PSET,  and  react  according to the color found. If, for example our 
spaceship encountered  an asteroid field which consisted of brown asteroids
,  we  could  check   all  the points around the spaceship every time it is 
moved  and,  if  a  brown pixel  was  detected,  an  explosion would occur. 
But  checking every point  around our sprite is very time consuming. A much 
better technique is to  check  the  most prominent points - 5 points at the 
nose,  5  at the tail and perhaps 10 points above and 10 points below. This 
would enable us to keep detecting while maintaining an adequate speed. 

Let's  take a look at this next piece of code which you can find at work in 
\CHAP12\CH12_5.BAS on the disk (which comes with the book):

 starcolor% = 2                                  'Select green stars
 FOR top% = Ship.X% TO Ship.X% + 10              '10 pixels above sprite
 pixelcolortop% = POINT(top%, Ship.Y% - 1)       'Get pixels color
 IF pixelcolortop% = starcolor% THEN             'Is it green?
 LOCATE 1, 1                                     'If it is...
 PRINT "Top hit!"                                '...then print this...
 GOTO Finishdetect                               '...and exit loop
 END IF                                          'Or else...
 NEXT top%                                       '...try next pixel
 FOR bottom% = Ship.X% + 5 TO Ship.X% + 15       '10 pixels below sprite
 pixelcolorbottom% = POINT(bottom%, Ship.Y% + 10)
 IF pixelcolorbottom% = starcolor% THEN
 LOCATE 1, 1
 PRINT "Bottom hit!"
 GOTO Finishdetect
 END IF
 NEXT bottom%
 FOR left% = Ship.Y% TO Ship.Y% + 5              '5 pixels left of sprite
 pixelcolorleft% = POINT(Ship.X% - 3, left%)
 IF pixelcolorleft% = starcolor% THEN
 LOCATE 1, 1
 PRINT "Left hit!"
 GOTO Finishdetect
 END IF
 NEXT left%
 FOR right% = Ship.Y% + 2 TO Ship.Y% + 7         '5 pixels right of sprite
 pixelcolorright% = POINT(Ship.X% + 25, right%)
 IF pixelcolorright% = starcolor% THEN
 LOCATE 1, 1
 PRINT "Right hit!"
 GOTO Finishdetect
 END IF
 NEXT right%
 Finishdetect:                                   'End detection

The listing is fairly straightforward and has the added bonus of ending 
detection immediately when a collision has been detected.

Good collision detection is essential and the lack of it is one the 
main reasons for games receiving poor comments from reviewers. Test
, test and test it again.

You can contact Wrox Press, the publishers of this book, at the following
places using the following methods:

 Editorial : Unit 16, 20 James Road, Tyseley, Birmingham, B28 0EE, UK
 Marketing : 2710 W. Touhy, Chicago, Illinois, 60645, USA
 Internet  : adrians@wrox.com   Or the Web Site: http://WWW.WROX.COM/

So after the 20th February where will you be? At the bookshop! 8-)







--------------------------------------------------------

*	EDITOR'S NOTE:
*	This article was originally printed in Peter Cooper's BASIX Fanzine, 
*	Issue #4 from January 1996.