----------
Face Lift
----------
	Don't you hate it when you are playing some game for Basic and 
you get a "Enter Speed (1-9000)"-type message. It is SO unprofessional. It
means some where in the program (probably at the end of the main loop) is
a little bit of code such as:

FOR delay = 1 to NumberTheyEnteredForSpeed
NEXT

	This is so the program won't go to fast on some computers. Thetas all 
well and good, but it is pointless to ask the user when you can do it 
yourself. It is like asking the user to seed the RANDOMIZE statement. But, 
if you don't ask, then how do you know how long to delay at the end of the 
main game loop you may wonder. Well that is easy too, here's what you do:

1-edit you code to this:
	...
Opening crap like DECLAREs and ON...GOTOs
	...
	TIMER ON                                        'add this

	MainGameLoop:
	start = TIMER                                   'add this
	...
	Your game logic stuff
	...
	FOR delay = 1 to NumberTheyEnteredForSpeed      'The delay loop
	NEXT
	LOCATE 1 : PRINT TIMER - start                  'add this
	GOTO MainGameLoop

2-Run the program many times, and enter a new number for speed each time 
  until you get a speed that is not to fast and not to slow for the game.
  Once you get it, look at top right corner of the screen and write down
  the number that keeps appearing there.
  (For the example's sake, we are going to say it was .34)

3-Re-edit your code to look like this
	...
Opening crap like DECLAREs and ON...GOTOs
	...
	TIMER ON

	MainGameLoop:
	start = TIMER
	...
	Your game logic stuff
	...
	DO : LOOP UNTIL TIMER - start > .34
	GOTO MainGameLoop

	Of course, your number probably won't be .34, so replace it with 
your number The DO : LOOP waits for .34 seconds from the start of the 
MainGameLoop until it makes another pass. 

4-How does this work?
Here is an Example:

Bob is playing PACMAN(tm) on an INTEL Pentium II  300mhz

	It takes the Pentium .09 seconds to go through the whole game loop. 
The program then sits in the DO : LOOP thingy for .25 seconds (.34 - .09=.25)
The program then goes back to the start of the Main Game loop and repeats.
 
Bill is Playing PACMAN(tm) on a 386sx 33mhz

	It takes the 386sx .20 seconds to go through the whole game loop. 
The program then sits in the DO : LOOP thingy for .14 seconds (.34 - .20=.14)
The program then goes back to the start of the Main Game loop and repeats.

No matter what Computer is used, the Main Game Loop is only run once every
.34 seconds. What happens if you have a really slow computer?

Dave is poor and has to run a copy of PACMAN(tm) that he stole on a 
IBM XT (c. 1984)

	It takes the piece-of-s...er...XT .8 seconds to go through the main 
game loop Then program hits the DO : LOOP thingy and says "Oh, .8 seconds 
have passed, so I don't have to wait at all in the DO : LOOP thingy, because 
it took me longer than .34 seconds because I suck. I'll just exit the DO : 
LOOP thingy and go back to the start of the main game loop!"

	This makes the program look better, and run the same on all computers.
Now, I know you will NEVER come across an old XT, 386s are all but dead, 
and 486s are slowly dying; but this is still helpful because a Pentium 75mhz 
is much slower then a Pentium MMX 166mhz which is slower than a Pentium II 
333Mhz.

--------------------------------------------------------------------------------
This tutorial originally appeared in the QBasic Developers Forum, Issue #1.
This was written by Lord Acidus.