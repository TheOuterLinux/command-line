     The QBNews                                                     Page 42
     Volume  1, Number  2                                 February  2, 1990

     DECLARE SUB OpenMenu ()
     DECLARE SUB MenuSelection ()
     DECLARE SUB TextMessages ()
     DECLARE SUB RotateSign ()
     DECLARE SUB TimeAMPM ()
     
     COMMON SHARED AMPM$, A$, B, C
     
     '=====
     
     'This little example is to show how you can have a 'menu' screen  with
     'various  selections, and also have rather detailed  information  also
     'presented to the user regarding each of his potential selections.  If
     'you  use some of the shareware fast screen prints, you can have  some
     'very  elaborate  instructions  on  the screen,  since  the  speed  of
     'printing  of  a  lot  of details does not become  a  factor  and  the
     'screenful of detailed information snaps into place.  The sample below
     'is  partially padded; for the values between #1 and #7.  It does  not
     'attempt to restrict a <RETURN> hit with no numeric selection nor some
     'other combinations.  It's purpose is to give you the basics of an  on
     'line additional information' from a menu.
     
     'This shows how simple the menu programming can be, yet obviously with
     'just  a  little work, many of QuickBasic's built-in material  can  be
     'used to clean up this listing.  The purpose here, was not to show how
     'well it can be programed, but to show that a programer who knows very
     'little  about BASIC language can still do some  pretty  sophisticated
     'programming.
     
     'SELECT  CASE is used in the A.M. / P.M. selection of the TIME so  you
     'would  need QB 4.5 or redo it with some IF Statements, and  then  you
     'could use this in nearly any BASIC.
     
     'YUP, there are a few line numbers.  Doubt, the world will come to  an
     'end.
     
     CLS
     CALL OpenMenu
     CALL MenuSelection                    'selections #1 - #7
     460 DO
               A$ = INKEY$
               IF LEN(A$) = 0 THEN         'nothing, so on to the screen
                    CALL RotateSign        'time / date / scrolling stuff
                    GOTO 470               'we want to continue
               END IF
     470       B = VAL(A$)                 'the original A$
               IF LEN(A$) = 0 THEN GOTO 460       'loop
               IF ASC(RIGHT$(A$, 1)) = 13 THEN GOTO 480 'a CR was hit,
                                           ' call the
                                           'routine, or run the module that
                                           'is numbered between 1 and 7
               A = B
               IF B < 1 THEN GOTO 460      'heave input values less than 1
               IF B > 7 THEN GOTO 460      'discard values over 7
     
     
     The QBNews                                                     Page 43
     Volume  1, Number  2                                 February  2, 1990

               CALL TextMessages
          LOOP
     
     480  COLOR 14 + 16, 1: LOCATE 22, 16
     
               'ON C GOTO 101 , 102 , 103 , OneO4 , 105 , 106 , AllDone
               '    101 RUN "ITEM1.EXE"
               '    102 RUN "ITEM2.EXE"
               '    103 ...
               '    OneO4 ...
               '    105 ...
               '    106 ...
               '    AllDone
     
          PRINT "You Pushed the RETURN and would also get Item #"; C
          COLOR 15, 4: LOCATE 23, 19
          PRINT "Please push CTL-BREAK to end this program"
          LOCATE 24, 10: COLOR 0, 0
          PRINT SPACE$(70);
     
          DO
          LOOP
     
     SUB MenuSelection
     
     Y = 25
     LOCATE 14, Y: COLOR 15, 1
     PRINT "[ 1 ] -- Item One"
     LOCATE 15, Y
     PRINT "[ 2 ] -- Item Two"
     LOCATE 16, Y
     PRINT "[ 3 ] -- Circles"
     LOCATE 17, Y
     PRINT "[ 4 ] -- No Circles"
     LOCATE 18, Y
     PRINT "[ 5 ] -- Item #5"
     LOCATE 19, Y
     PRINT "[ 6 ] -- Item #6"
     LOCATE 20, Y
     PRINT "[ 7 ] -- Item #7"
     END SUB
     
     SUB OpenMenu
     
     LOCATE 6, 1: COLOR 7, 1
     PRINT "   This little program shows how you may place on the screen a_
     short     "
     PRINT  "    instruction  or greater information based  upon  the  key_
     selected by     "
     PRINT  "    the  user.  This information stays on  the  screen  until_
     another possible"
     PRINT "   key is selected, or the <RETURN> key is tapped, which takes_
     you to    "
     PRINT  "   the item that is CALLED by the progam associated with  the_
     input key.  "
     
     The QBNews                                                     Page 44
     Volume  1, Number  2                                 February  2, 1990

     COLOR 12 + 16, 1
     PRINT  "                       Tap any key; but preferably  #1  -  #7_
     "
     END SUB
     
     SUB RotateSign
     
          S1$ = "Select a numeric item and then tap <RETURN>"
          Z1 = LEN(S1$)
          S$ = "  Be Sure You Have Made Adequate Backup Disks!!  "
          Z = LEN(S$)
          DO UNTIL I = LEN(S$)
               A$ = INKEY$                'this is WITHIN the first INKEY$
               IF A$ > STR$(0) THEN EXIT SUB
               IF LEN(A$) = 0 THEN GOTO 33
               IF ASC(RIGHT$(A$, 1)) = 104 THEN EXIT SUB
               IF ASC(RIGHT$(A$, 1)) = 13 THEN EXIT SUB     'a CR was hit
     33        StartTime = TIMER
               IF I = LEN(S$) - 1 THEN I = 0
               LOCATE 12, 16: COLOR 15, 4
     
               DO UNTIL EndTime - StartTime >= .1
                    EndTime = TIMER
               LOOP
     
               PRINT LEFT$(S$, LEN(S$) - I)       'scrolling upper message
               LOCATE 12, 16: COLOR 15, 4
               PRINT RIGHT$(S$, Z - (Z - I))
               Y = Y + 1
               IF Y = 15 THEN Y = 8
               COLOR 15, 0
               LOCATE 24, 18
               PRINT RIGHT$(S1$, LEN(S$) - 2 - I);'scrolling bottom message
               I = I + 1
               CALL TimeAMPM
               LOCATE 22, 25: COLOR 15, 0: PRINT AMPM$
               LOCATE 22, 45: PRINT DATE$
          LOOP
     END SUB
     
     SUB TextMessages
     LOCATE 3, 10
     PRINT SPACE$(70)
     LOCATE 4, 10
     PRINT SPACE$(70)
     LOCATE 3, 10
     C = B
     ON B GOTO 1, 2, 3, 4, 5, 6, 7
     1 COLOR 14, 2
          PRINT "          You are considering selecting item #1        "
          EXIT SUB
     2 COLOR 12, 1
          PRINT "          This could be a reminder for item #2         "
          LOCATE 4, 10
          PRINT "  This also shows you are NOT restricted to line quantity"
     
     The QBNews                                                     Page 45
     Volume  1, Number  2                                 February  2, 1990

          LOCATE 4, 36: COLOR 15 + 16, 1: PRINT "NOT"
          EXIT SUB
     3 COLOR 15, 9
          PRINT "     If you select this item, you will run in circles "
          EXIT SUB
     4 COLOR 14, 6
          PRINT "       Obviously this is not for going in circles  "
          EXIT SUB
     5 COLOR 11, 10
          PRINT "  Maybe you have an area that should be called by item #5"
          EXIT SUB
     6 COLOR 13, 2
          PRINT  "The sixth item on the menu might have  this  informative_
     message     "
          LOCATE 4, 10
          PRINT  "This  could  be  an entire window,  snapped  on  with  a_
     SHAREWARE program"
          EXIT SUB
     7 COLOR 11, 6
          PRINT " Maybe item #7 could be an instruction to end the program"
          EXIT SUB
     END SUB
     
     SUB TimeAMPM                  'a 12 hour clock with either AM or PM at
     the end
          AMPM$ = LEFT$(TIME$, 2)
               SELECT CASE VAL(AMPM$)
               CASE IS >= 12
                    AMPM = VAL(AMPM$) - 12
                    AMPM$ = STR$(AMPM) + RIGHT$(TIME$, 6) + " PM"
               CASE IS < 12
                    AMPM$ = TIME$ + " AM"
               CASE ELSE
          EXIT SUB
          END SELECT
     END SUB
     
     
     by Don Avila
     Compuserve 71525,2041
     
     [EDITOR'S NOTE]
          I may have screwed up the spacing with my word processor.  Sorry.
     David Cleary