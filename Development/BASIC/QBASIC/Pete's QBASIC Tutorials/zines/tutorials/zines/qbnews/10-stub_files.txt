     The QBNews                                                     Page 12
     Volume  3, Number  1                                    March 29, 1992

     Using Stub Files to Reduce EXE Size by David Cleary
     
     Microsoft BASIC Professional Development System version 7.1 comes with
     many "stub" files to help reduce the size of your EXE programs. By
     linking with these stub files under certain conditions, you will see
     reductions in EXE size of 2 - 15K. However, improperly using stub
     files may result in your EXE actually getting larger.
     
     Because of the complexity of the BASIC run time libraries, many times
     routines will be included in your EXE even though they are never used.
     For instance, when you do this:
     
     OPEN A$ FOR RANDOM AS #1
     
     the compiler doesn't know if A$ is referring to a file, or to the
     communications port. Therefore, the compiler has no choice but to
     include all the code to handle the communications port. What a stub
     file does, in this instance NOCOM.OBJ, is replace the code that is not
     needed with an empty procedure. This causes all external references
     to be resolved and cause a decrease in EXE size.
     
     The tricky part is figuring out when to use them. For instance, if you
     changed A$ in the example above to:
     
     OPEN "C:\INFO.DAT" FOR RANDOM AS #1
     
     the compiler knows you want to open a file. It will not include the
     communications routines in your EXE file. If you try and link with
     NOCOM.OBJ, you will actually see an increase in EXE size.
     
     There are two types of stub files provided with PDS 7.1. The first
     type is used with stand alone programs (compiled with /O) are are used
     to reduce the size of your EXE file. They include:
     
     NOCGA, NOEGA, NOOGA, NOHERC, NOVGA, NOGRAPH  Graphics stub files
     NOCOM, NOLPT  Device support stub files
     SMALLERR  Error message stub file
     87.LIB  Floating point emulation stub file
     NOEMS, OVLDOS21  Overlay support stub files
     NOFLTIN  Floating point input support stub file
     NOEDIT  INPUT editting functions stub file
     TSCNIO  Text output stub file
     NOTRNEMx.LIB  Transcendental operation stub file
     
     The second type of stub file is used when creating custom run time
     libraries. They do not decrease the size of your EXE file, but rather
     decrease the size of your BRUN file. They include:
     
     NOEVENT  Event trapping stub file
     NOISAM  ISAM support stub file
     
     The following instructions on correctly using these stub files came
     from a file located in the Microsoft Software Library on Compuserve.
     You can access the Microsoft Software Library by typing GO MSDS at any
     
     The QBNews                                                     Page 13
     Volume  3, Number  1                                    March 29, 1992

     ! prompt.
     
     When to Use the NOxxx.OBJ Graphics Stub Files
     --------------------------------------------- 
     
     When you code any graphics SCREEN mode, the compiler knows to generate
     code that will require support only for that mode. Therefore, the
     linker will only link in support for the screen mode that you are
     using. If you only use SCREEN 9, the linker will only pull the EGA
     graphics support out of the BASIC libraries.  
     
     The one time when all graphics support will be linked with your
     program is if you use SCREEN with a variable such as the following: 
     
     
        SCREEN X% 
     
     In this case, the compiler does not know what graphics mode you will
     be using until run time. Therefore, it is forced to generate code that
     will cause ALL of the BASIC graphics support to be linked in. In this
     case, you would use NOGRAPH.OBJ, or one or more of the following stub
     files, to stub out support for those modes you will not be using: 
     
     
        NOCGA.OBJ, NOEGA.OBJ, NOOGA.OBJ, NOHERC.OBJ, NOVGA.OBJ 
     
     NOGRAPH.OBJ would only be useful if you are using SCREEN X%, where X%
     is guaranteed to be zero. NOGRAPH.OBJ is only useful when building
     custom run-time modules that will not support graphics. NOGRAPH is a
     superset of all the other above graphics stub files and cannot be
     linked with any of them.  
     
     When to Use NOCOM.OBJ or NOLPT.OBJ
     ----------------------------------
     
     Normally, support for communications or printing will not be linked in
     unless an OPEN "COMx:" or OPEN "LPTx:" statement exists in the
     program. However, if the program contains a statement that resembles
     the following 
     
     
        OPEN A$ FOR <mode> as #n 
     
     then the compiler must assume the A$ might be equal to "COMx:" or
     "LPTx:" at some time during the program. Therefore, communications and
     printing support will be linked in along with all other file support.
     If A$ will never be used to open the "COMx:" port or the printer, then
     the NOCOM.OBJ support module can be used to stub out support for
     communications and NOLPT.OBJ can be used to stub out printer support.
     This should reduce the size of the .EXE program.  
     
     When to Use SMALLERR.OBJ
     ------------------------
     
     
     The QBNews                                                     Page 14
     Volume  3, Number  1                                    March 29, 1992

     SMALLERR substitutes smaller error message text strings for the normal
     error messages and maps the BASIC error messages down to a few
     messages. Use this only in a program known to be very stable.  
     
     When to Use 87.LIB
     ------------------
     
     87.LIB could increase the size of the .EXE if the program does not
     contain any math statements at all. However, use the 87.LIB stub file
     (in the .OBJ argument list of LINK, even though it is a .LIB file) to
     decrease the size of the .EXE file when the program contains a math
     statement and will be run ONLY on machines with a math coprocessor.  
     
     When to Use NOEVENT.OBJ
     -----------------------
     
     The NOEVENT.OBJ stub file is useful only to reduce the size of a
     customized run-time module that will not support event trapping.
     NOEVENT.OBJ is NOT useful for reducing the size of your compiled BASIC
     .EXE program.  
     
     If the program can be compiled without /V and /W, the program doesn't
     require event trapping. In this case, mistakenly linking your BASIC
     .OBJ modules directly with NOEVENT.OBJ may increase program size. If
     the program requires /V or /W, you would also not want to link with
     this stub file, since NOEVENT.OBJ is only useful for making customized
     run-time modules.  
     
     When to Use NOEMS.OBJ
     ---------------------
     
     In an overlaid program, this stub file will prevent the overlay
     manager from using expanded memory (EM) even if EM is present. This
     stub could be used if the program needed to use EM for other purposes
     (such as ISAM) and did not want the overlay manager to use EM. This
     stub file reduces code size only by about 1000 bytes. Do not use this
     stub file if the program does not contain overlays or if it is an OS/2
     protected mode program.  
     
     OVLDOS21.OBJ
     ------------
     
     This stub file adds support for DOS version 2.10 to an overlaid
     program. Any overlaid program that would want to work on DOS 2.10 must
     have this file linked with it. OVLDOS21 is not needed (and should not
     be used) if the program is not overlaid or if it is a protected mode
     program. The stub file does not remove code, but always adds code and
     functionality (about 500 bytes).  
     
     When to Use NOFLTIN.OBJ
     -----------------------
     
     Replaces the INPUT code with an "integer only" version. If you link
     with this stub file, all forms of numeric input (INPUT, INPUT #, VAL,
     
     The QBNews                                                     Page 15
     Volume  3, Number  1                                    March 29, 1992

     and READ) will have the following restrictions: 
     
     1. READ, INPUT, INPUT #, and VAL will no longer recognize leading base 
        specifiers on numbers. The following specifiers are illegal: 
     
     
           &H &O, & 
     
     2. READ, INPUT, INPUT #, and VAL will no longer recognize trailing 
        type specifiers on numbers. The following specifiers are illegal: 
     
     
           !, %, #, & and @ 
     
     3. READ, INPUT, INPUT #, and VAL will no longer parse a number with 
        either a decimal point, E, or D in them. Thus, while 1E3 is a legal 
        BASIC integer, it will not be recognized.  
     
     4. Using a SINGLE precision, DOUBLE precision, or CURRENCY variable 
        with READ, INPUT, INPUT #, and VAL will cause the math support to 
        be linked in. The restrictions in points 1-3 above will still hold.  
        Also, using any other math related operation will pull in the 
        support pack without lifting the restrictions of 1-3.  
     
     The main size advantage to using this stub file occurs when READ,
     INPUT, INPUT#, or VAL are the only statements that use the math
     package. This stub file will then eliminate at least 11K of code. If
     some other statement also needs the math package, the savings will be
     less than 2K.  
     
     When to Use NOEDIT.OBJ
     ----------------------
     
     This stub file replaces the editor used with the INPUT and LINE INPUT
     statements. It removes the following functions from the editor: 
     
     1. No control character commands are recognized except for ENTER (done 
        editing) and BACKSPACE (removes the last character). All other 
        control characters are ignored, as are INS, DEL, HOME, END, and the 
        arrow keys. Function keys will still expand because this 
        functionality is performed outside the INPUT editor.  
     
     2. If output is redirected (using >) and input is not redirected (no 
        <), no characters will be echoed to the screen. The normal INPUT 
        editor will print the characters as you types them in this 
        situation.  
     
     3. No characters will be echoed to the line printer even if line 
        printer echoing is turned on.  
     
     You can obtain a 1K reduction in .EXE size using this stub file.
     Linking with this file could increase program size if the program
     never uses INPUT, LINE INPUT, LINE INPUT #, RANDOMIZE. If the program
     does not have INPUT or LINE INPUT statements but does have LINE INPUT
     
     The QBNews                                                     Page 16
     Volume  3, Number  1                                    March 29, 1992

     # or RANDOMIZE (with not parameters), there will still be some
     savings.  
     
     When to Use the TSCNIOxx.OBJ Stub Files
     --------------------------------------- 
     
     Note: The statements affected by this stub file are subject to change
     in future versions.  
     
     The TSCNIOxx.OBJ file provides a smaller, simpler text output package.
     When it is linked in, the following differences from standard BASIC
     will occur: 
     
     
      1. No graphics statements are supported. All the following statements 
         and functions will generate an illegal function call: 
     
     
            CIRCLE, POINT statement, POINT function, PAINT, PSET, VIEW,
            VIEW SCREEN, WINDOW, PALETTE, PALETTE USING, DRAW,
            PUT (graphics), LINE, PCOPY, PMAP, GET (graphics)
     
     
      2. VIEW PRINT will not be supported. All programs will have an 
         implicit VIEW PRINT 1 TO <screen-size - 1> as is the normal 
         default. For programs linked with this stub file, the only way to 
         reach the bottom screen line is with the LOCATE statement, since 
         the VIEW PRINT statement is not available.  
     
     
      3. There will be no support for function key display. The KEY 
         ON/OFF/LIST statements will generate an illegal function call.  
         CTRL+T to an INPUT statement will be silently ignored. Softkey 
         definition and expansion will still be supported.  
     
     
      4. The COLOR statement will not accept a border parameter. Also, the 
         color parameters are sent directly to the hardware. Thus, on an 
         EGA or VGA card with a monochrome monitor, many values for COLOR 
         will cause text to not be visible.  
     
     
      5. Embedded control characters will not be supported for PRINT.  
         Currently, the following features (which need to be added to the 
         manual) are supported for the nonstubbed version of the PRINT
         code:
     
     
         Control 
         Character   Description 
         ---------   ----------- 
     
     
         CTRL+G      Beeps the speaker 
     
     The QBNews                                                     Page 17
     Volume  3, Number  1                                    March 29, 1992

     
     
         CTRL+I      Moves the cursor to the next tab position, erasing 
                     characters 
     
     
         CTRL+J      Moves the cursor to the beginning of the next line 
     
     
         CTRL+K      Moves the cursor to the top-left corner of the current 
                     VIEW PRINT window 
     
     
         CTRL+L      Clears the VIEW PRINT window, homes the cursor within 
                     it 
     
     
         CTRL+M      Moves the cursor to the beginning of the next line 
     
     
         CTRL+\      Moves the cursor to the right one space, stopping at 
                     the end of the line 
     
     
         CTRL+]      Moves the cursor to the left one space, stopping at 
                     the beginning of the line 
     
     
         CTRL ^      Moves the cursor up one line, stopping at the top of 
                     the VIEW PRINT window 
     
     
         CTRL_       Move the cursor down one line, stopping at the bottom 
                     of the VIEW PRINT window 
     
     
      6. The SCREEN and WIDTH statements will not be supported. The program 
         will use whatever screen mode was available on start-up, unless it 
         is a graphics mode, in which case an attempt will be made to 
         determine if any text mode can be run in that graphics mode. The 
         screen will always be 80 columns, displaying on video page 0. The 
         number of lines will be determined by start-up conditions. The 
         screen state will not be restored when the program terminates.  
     
     
      7. CLS will not accept any parameters.  
     
     
      8. Underline cursors on an EGA card not in 25-line mode may not be 
         handled properly.  
     
     
      9. This stub file may not be used to build a customized run-time
         module. It may not support the CHAIN statement.
     
     The QBNews                                                     Page 18
     Volume  3, Number  1                                    March 29, 1992

     
     10. This stub file does not support CGA snow elimination during screen 
         output.  
     
     This "set" of stub files is dependent upon operating system and
     near/far string support. Thus, there are four different versions of
     the file: 
     
     
        File           Description 
        ----           ----------- 
     
     
        TSCNIONR.OBJ   Near string DOS version 
        TSCNIOFR.OBJ   Far string DOS version 
        TSCNIONP.OBJ   Near string OS/2 version 
        TSCNIOFP.OBJ   Far string OS/2 version 
     
     Any program linked with one of these stub files should decrease in
     size. The size decrease should be between 1.3K and 4.4K depending on
     the statements used in the program and the library being linked with.  
     
     When to Use the NOTRNEMx.LIB Stub File
     -------------------------------------- 
     
     NOTRNEMR.LIB and NOTRNEMP.LIB remove support for transcendental
     operations, including: SIN, COS, TAN, ATN, LOG, SQR, EXP, ^ (the
     exponential operator), a CIRCLE statement with a start or stop value,
     and the DRAW statement with the A or T commands.  
     
     When to Use the NOISAM.OBJ Stub File
     ------------------------------------ 
     
     NOISAM.OBJ removes ISAM functionality from customized BASIC run-time
     modules. NOISAM.OBJ is not useful when linking stand-alone (BC /O)
     BASIC executable programs.
     
     ======================================================================
     David Cleary is a software engineer for Vectron Laboratories in
     Norwalk, CT and is also the author of Crescent Software's PDQComm. He
     can be reached in care of this newsletter, on Compuserve as
     76510,1725, on Prodigy as HSRW18A, on Fidonet at 1:141/777, and on the
     Crescent Software Support BBS at 203-426-5958.
     ======================================================================