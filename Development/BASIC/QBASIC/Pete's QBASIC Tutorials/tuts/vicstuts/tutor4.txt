


                     Vic's QBasic Programming Tutorial

                           Basic Tutorial IV

                           MOUSE CONTROL!!!!



-----------------------------------------------------------------------------

	Have you ever seen a QBasic program that uses the mouse?  Have you
also wondered how you could do this easily and quickley?  Well, here you
go...

First, let me explain how it is posible to access mouse control in Programm-
ing.  Most likeley you have no Idea about ASM (Assembly) Programming... 
I will admit that learning that can be extremely hard, but there is one
easy thing to get from it... It is something that is used in many many
DOS programming languages.  It is called INTERUPTS...  When you use an
interupt you are telling the computer itself to do something.  A qbasic
program calls interupts all the time, only you don't know it and can't realy
controll it.  
	In order to use an Interupt you have to put some information into
memory and call an interupt number.  If you look in a book of interupts you
would find that the mouse interupt is number 33.  Before you can use this
you need to tell the computer what you want it to do with the mouse by
putting a number into memory.  Here is a list of some of the numbers and
what they do. (you can find this in any INTERUPT book...)

0  RESET DRIVER AND READ STATUS
1  SHOW THE MOUSE
2  HIDE THE MOUSE
3  GIVES THE MOUSE X,Y AND BUTTON STATUS
4  SETS THE MOUSE POSITION
5  BUTTON PRESSED DATA
6  BUTTON RELEASED DATA
7  DEFINE HORIZONTAL RANGE (MAX X)
8  DEFINE VERTICLE RANGE   (MAX Y)

That is all of the important ones that we will use.

In ASM turning on the mouse it would look like this...

MOV AX,1
INT 33

Thats it... Very simple, you move(MOV) 1 into the memory(AX) and call the
interupt(INT) 33 and BANG! The mouse is showed on the screen.
In Assembely this is very simple, but in QBASIC it gets a bit more involved.
You can't just use MOV and INT to call things...  I will get into ASM in 
QBasic in a later tutorial.  For right now I just want you to copy the
code... (Very simple)...

------------------------------------------------------
Every time I use the mouse I use this program...
To copy this, 

1. open up a text editor (NotePad.exe) 
2. highlight the bottom code (Start where it says to...).
3. Goto Edit then Copy 
4. Goto the new page in the text editor...
5. Goto Edit and then Paste.
6. Goto File and then Save_As...
7. Goto the Save As type box and change it to "All Files (*.*)"
8. Find the C:\ directory (or whatever directory you want, this example will
   use the C:\ directory...)
9. Goto the File Name: box and type Mouse.Bas...
10. Click Save...
11. Get into QBasic
12. Load the c:\Mouse.bas...
13. Run the program...
14. Move and click the mouse, notice what changes...

In most cases this works.  If you are using Qbasic 4.0 or higher you must
start QBasic in a DOS prompt by typing 
DIRECTORY:\qb /l
But, I would REALY RECOMEND using The original QBASIC...  It should be in
the same directory as QBasic but it would be called OLDQB.exe, I think...
Heres the code... 




'-------------| START MOUSE PROGRAM (START COPYING HERE)--------------------

DEFINT A-Z
DECLARE SUB mouse (cx, dx, bx)
DECLARE SUB MousePointer (SW)
DIM SHARED a(9)                 'Set up array for code
DEF SEG = VARSEG(a(0))          'Get array segment (nnnn:    )
                                 '    (two 8 bit)
    FOR i = 0 TO 17                 'length of DATA to
       READ r                       'read
       POKE VARPTR(a(0)) + i, r     'into array/2 (nnnn:iiii) (one 8 bit)
    NEXT i                          'until 17

'**************************** Machine Code *********************************

DATA &HB8,&H00,&H00   :   ' mov  AX,[n]       [Swap code-(L),(H)] in AX
DATA &H55             :   ' push BP           Save BP
DATA &H8B,&HEC        :   ' mov  BP,SP        Get BP to c Seg
DATA &HCD,&H33        :   ' int  33           Interrupt 33
DATA &H92             :   ' xchg AX,[reg]     [Swap code-reg] in AX
DATA &H8B,&H5E,&H06   :   ' mov  BX,[BP+6]    Point to (variable)
DATA &H89,&H07        :   ' mov  [BX],AX      Put AX in (variable)
DATA &H5D             :   ' pop  BP           Restore BP
DATA &HCA,&H02,&H00   :   ' ret  2            Far return

SCREEN 13
'****************************** Mouse set up ******************************
           
                CALL MousePointer(0)      'Reset mouse and
                CALL MousePointer(1)      'turn pointer on
                CALL MousePointer(3)      'Get coordinates

'****************************** P R O G R A M ******************************
DO           'Put your code here
CALL mouse(cx, dx, bx)
LOCATE 1, 1: PRINT dx; cx; bx
LOOP UNTIL INKEY$ = CHR$(27) 'Stop your code here
END

SUB mouse (cx, dx, bx)
         
           POKE VARPTR(a(4)), &H92           'Swap code,Get CX setup
          CALL absolute(cx, VARPTR(a(0)))     'Run Code
            '  cx = cx / 8                     'Adjust 25x80
           POKE VARPTR(a(4)), &H91           'Swap code,Get DX setup
          CALL absolute(dx, VARPTR(a(0)))     'Run Code
              dx = dx / 2                     'Adjust 25x80
           POKE VARPTR(a(4)), &H93           'Swap code,Get BX setup
          CALL absolute(bx, VARPTR(a(0)))     'Run Code

                                   'Note :
                                   'Remove the /8
                                   'for graphics modes.

END SUB

SUB MousePointer (SW)
         
           POKE VARPTR(a(0)) + 1, SW         'Swap code,Set AX = (SW)
          CALL absolute(c, VARPTR(a(0)))     'Run Code

                                          'Note:
                                             'SW = 0-reset
                                             'SW = 1-on
                                             'SW = 2-off
                                             'SW = 3-coordinates


END SUB


'-------------------| THE END (Cut here)|----------------------------------



Wow huh?!
I realy hope this works for you, I tested it out on my computer and it 
works... Just remember to use the original QBASIC...

You can ignore everything except for the DO LOOP Part
This is where you will put your program...

'---------------------------------------------------------------------------

You should know what you need to do from here to work on your own mouse
project.  You just use basic colide detection tecniques...

here is an example for you

In the above program bellow the 

LOCATE 1, 1: PRINT dx; cx; bx

and above the

LOOP UNTIL INKEY$ = CHR$(27) 'Stop your code here

Type this in

'...

LINE (50, 50)-(80, 70), 15, B
IF bx = 1 THEN
IF dx > 50 AND dx < 80 THEN
IF cx > 50 AND cx < 70 THEN
LOCATE 2, 1: PRINT "You clicked in the box"
END IF
END IF
END IF

'...

What this does is draw a box that you can click in...
when you click in the box it will tell you...

----------------------------------------------------------------------------

I know this was a very short tutorial, but that is all you should really
need to start your own mouse programs.  There are many QLB files out there
that do the same thing with much less code, but I prefer to use the old
fashioned way to use the mouse...
Before I leave you I should discuss a few more things...

1.  If you want to turn the visible mouse off so you can draw your own
    cursor at the point the mouse is at use the command

		CALL MousePointer(2)
		CALL MousePointer(3)

     With this you are still able to tell the mouse coordinates but you do
     not see the mouse.  this is good if you want to make your own mouse 
     cursor without having to screw around with ASM and bitmaps (if you
     know what I mean)...

2.  If you draw something over the mouse and then move the mouse you might
    notice that the mouse breaks through the background and destroys what
    was drawn.  To fix this you do this
    
    CALL MousePointer(2)   'This turns the pointer off
    'Draw your picture
    CALL MousePointer(1)   'This turns the pointer back
    CALL MousePointer(3)   'This grabs the coordinates

      That should solve that problem... If it doesn't mess around with it...

3.  The CALL MousePointer(x) commands
    
    0 - Resets the mouse
    1 - Turns mouse on
    2 - turns mouse off
    3 - gets the mouses coords...

With this in mind you shouldn't have any problems working the mouse...
Thanks for reading my tutorial, and I really hope I didn't insult your
intellegence...
---------------------------------------------------------------------------

My current E-Mail address is RADIOHANDS@AOL.com

If you are using this tutorial on your page, please leave the tutorial 
exactly as it is... please don't change anything, unless its spelling
errors... Theres alot of them! I don't like using the backspace key...

The original website that these were on is

http://members.aol.com/radiohands/index.html

Thank you
Vic Luce
Finished
November 1
1999

If you want to be notified when a new tutorial is out..
Send An E-mail to RADIOHANDS@AOL.com
with the subject saying VQBLIST and then your E-mail address(check website)
