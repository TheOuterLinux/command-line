
                     Vic's QBasic Programming Tutorial

                           Basic Tutorial XVI

                           Making QB Libraries...
                     Make QB your very own language...
                           Radiohands@aol.com

-----------------------------------------------------------------------------

Ok, firsts first...  What is a Qbasic Library (.QLB)?

For those of you who have been lucky enough to find a copy of Qbasic 4.5
on the internet, or from a freind, there is a way to make programming
in qbasic quicker and easier than ever...

!!| NOTE |!!
If you do not have Qbasic 4.5 or higher than you have no need to read this
tutorial, unless you are curious and want to know what its all about
!!|       |!!

What is QB 4.5?
Qbasic 4.5 is a version of Qbasic that can make your *.bas files into
executable *.exe's...  and as a bonus feature it allows you to use
library files, to improve, or make new qbasic commands...
This feature is also in Qbasic Extended (qbx or qb 7.1)...

This is not a freeware version of qbasic...  But, you can't buy it from
microsoft anymore...

NOTE!!
there are places you can find Qbasic 4.5 for sale, but there are a few
things that you should know about this...

1.)  It is very expensive...
2.)  It is illegal...
3.)  It just plain stupid...

NOTE!!

In no way do I want to give the hint that you should find QB 4.5 on the
internet, because it is illegal, If you know of a friend that has this on
his/her computer legally, then I would Use his/hers... Thats what all QB 4.5
programmers do anyway, right?...


-----------------

A program library is a file that has a list of new ( or improved ) commands.
In order to use the library, you have to include it in QB, and then list
the commands...

You can make your own qbasic library files, and I will include that in
my next tutorial...

There are a few ways to make a qb library file...

1.)  Use ASM...
2.)  Use a SUB that is in another *.bas file...
3.)  Use C++ or C Include / header files and convert them...

I don't know of anyothers, but I am sure their are others...

---------------------------------------------------------------------------




Ok, now, you have qbasic 4.5 right?  Now what do you need?
You need a Qbasic Library file...
You can find them all over the net, you may have heard of a few of them...

1.) DQB (direct Qbasic) "http://ec.quickbasic.com"
2.) Future Library "http://www.qb45.com"

Those are just a few examples of the hundreds of Qbasic library files out
there...

But for this tutorial, I will use My qbasic library...
!!Note!!
The method used to get my qb library to work will work for allmost
any qbasic library file out there...
!!!!


Ok, first you have to download the file...
Right now (2/22/2000) the file is located here...
http://members.aol.com/radiohands/libtut.zip
(there is also a link to it on the tutorial site...)

after you download the file extract all the files into your QB 4.5 directory
there should be 3 files...

vqb.bi
vqb.qlb
vqb.lib
...


-----
now, after you extract the files into the qb 4.5 directory
open up a dos window "command.com" and go to (CD c:\directory) the directory
where you extracted the vqb files...

then, when you are in that directory type in this command

qb /lvqb

and hit enter...


Qb should start...
When you are in qbasic put this command on the top of the new file...

REM $INCLUDE: 'vqb.bi'
or you could put
'$INCLUDE:'vqb.bi'


---

Now that you are all set up and ready to program using my library I have
to tell you a few of the commands that are in the programming library...

First, open up the file "vqb.bi" in a text editor (notepad.exe)...

you should see this...


--------------------------------------

DECLARE SUB scr13 ()
DECLARE SUB DOSSCREEN ()
DECLARE SUB scr101 ()
DECLARE SUB modex ()
DECLARE SUB mouseon ()
DECLARE SUB mouseoff ()
DECLARE SUB mousexy (BYVAL x%, BYVAL y%)
DECLARE SUB vqbpset (BYVAL x%, BYVAL y%, BYVAL c%)
DECLARE SUB vpset (BYVAL x%, BYVAL y%, BYVAL c%)
DECLARE SUB mxpset (BYVAL x%, BYVAL y%, BYVAL c%)
DECLARE SUB mxppset (BYVAL p%, BYVAL x%, BYVAL y%, BYVAL c%)
DECLARE SUB flip (BYVAL f%)
DECLARE FUNCTION mousex% ()
DECLARE FUNCTION mousey% ()
DECLARE FUNCTION mouseb% ()
DECLARE SUB scroll (BYVAL x%)
DECLARE SUB vqbclose ()

---------------------------------------

Those are all the commands that are in the Library...

!!NOTE!!
This is just a stupid library that I worked on and have not paid much
attention to...  Things may be wrong, I don't know...  Some may or may
not work...
!!!!


Look at the first commands...


DECLARE SUB scr13 ()
DECLARE SUB DOSSCREEN ()
DECLARE SUB scr101 ()
DECLARE SUB modex ()

These controll what screen mode you might want to enter...

here they are respectivly...

scr13 ()
call it like this...
---
scr13

what it does...
---
brings you into screen mode 13...

-----------------------------------------------

DECLARE SUB DOSSCREEN ()
call it like this...
---
DOSSCREEN

what it does...
---
brings you into the old DOS screen # 3... (Natural dos screen)


-----------------------------------------------

DECLARE SUB scr101 ()
call it like this...
---
scr101

what it does...
---
brings you into SVGA screen mode 640 x 480 - 256 colors
you can't plot pixels yet, This was just a test...


-----------------------------------------------

DECLARE SUB modex ()
call it like this...
---
modex

what it does...
---
brings you into MODEX 320 x 200 with 4 pages...
you can plot pixels, but you can't use the pages... yet...


----------------------------------------------------------------------------


First lets go into modex...

youre complete program should look like this...

'------

REM $INCLUDE: 'vqb.bi'

modex

'--------

You are now in modex...  Its that easy... no worry about large SUB files...



--------------------------

Since modex is not supported by qbasic, you can't use PSET (x,y),c to
plot a pixel in modex... you have to use the one I added in the library...

It looks like this in the vqb.bi file...

DECLARE SUB mxpset (BYVAL x%, BYVAL y%, BYVAL c%)

so, in order to use it you must use it like this...

Call mxpset (x,y,color)

Now, lets add that to our program...



'------

REM $INCLUDE: 'vqb.bi'

modex
call mxpset (50,50,4)

'--------

That should put a red pixel at 50 x 50 on the screen...


---------------------------------------------------------------------------
Ok, that was a boring example (unless you know what modex is...)

lets try some of the Functions that I put in...

---

DECLARE FUNCTION mousex% ()
DECLARE FUNCTION mousey% ()
DECLARE FUNCTION mouseb% ()

---

These functions tell you the x and y value, and the value of the button...

Ok, erase the old MODEX program...
Not the REM $INCLUDE:...

first, we have to turn on the mouse... use this command...

mouseon

second, we have to make a loop so the program doesn't end...
do it like this...



Do:press$ = inkey$

loop until press$ = chr$(27)
END



Ok, now lets discuss what a function is...

-----------

with a sub you call it like this...
call sub

with a function you call it like this...

X=function

the value that the function returns will be stored in X

!!NOTE!! the value does not allways have to be x... thats just an example...

so, in order to find the mouses X value, we have to store it in an array...

So, in the middle of the loop put this...

MX = mousex

That calls the mousex function and stores the value of the mouses X position
into the array MX, so MX now equalls the x value of the mouse...
I think you understand that, now lets finish the example...











'----------------- Mouse Example -----------
REM $INCLUDE:'vqb.bi'

SCREEN 13
Mouseon

Do:press$ = inkey$
MX = mousex
MY = mousey

locate 1,1:print mx;my

loop until press$ = chr$(27)
END

'------------ END Example...


Look at that, In my old MOUSE tutorial it took loads and loads of code to
get that simple effect...  With a library, you just use a few commands...





*/*\*/*/*\*/*/*\*/*/*\*/*/*\*/*/*\*/*/*\*/*/*\*/*/*\*/*/*\*/*/*\*/*/*\*/


Ok, now we have discussed how to get my library to work, lets describe how
to get others to work too...


Lets talk about DQB...
you can download DQB from their website
http://ec.quickbasic.com

but the library file is in its source code, so you have to compile it
yourself... Just follow what is says to make the library and your ready to
go...

Now, when you are ready to use the library, there is something you should
know...

DQB and many other libraries use What is called EXTENDED MEMORY (EMS)
Every computer has it, but it is not allways set up, sometimes you have to
set it up on your own...  This is how you do it...

Open up your Config.sys file in a text editor...
open notepad.exe up and find the file C:\config.sys and open it...

look through the file until you get to the bottom...
Add this to the bottom...

DEVICE=C:\WINDOWS\EMM386.EXE RAM 4000 X=CA00-D7FF

And restart your computer...
EMS is all set to run on your computer...
NOTE!! don't do this if you allready have EMS set up...

---
Now back to DQB

go to a dos prompt (command.com) and CD to your Folder where Qb is...

type in 

qb /ldqb 
and hit enter...

now, at the top of the new file type this...

REM $INCLUDE: 'directqb.bi'

Now you are all set to use the library...
Information on using the library can be found on the DQB site...

This should be all you really need to get QB to run with libraries...
I hope this helped...

In my next tutorial, I plan to tell you how you can use ASM to make your
own Qbasic Library file...





-----------------------------------------------------------------------------

Thats it for this tutorial, If I didn't get into enough detail in the
explanations then just look at the source code and try to figure it out
on your own.  All else fails E-Mail Me...  I want to make these tutorials
as easy to understand as posible!!!

My current E-Mail address is RADIOHANDS@AOL.com

If you are using this tutorial on your page, please leave the tutorial 
exactly as it is... please don't change anything, unless its spelling
errors... Theres alot of them! I don't like using the backspace key...

The original website that these were on is

http://members.aol.com/radiohands/index.html

Thank you
Vic Luce
Finished
Febuary 23
2000

If you want to be notified when a new tutorial is out..
Send An E-mail to RADIOHANDS@AOL.com
with the subject saying VQBLIST and then your E-mail address(check website)



