'___
' |he Code Post 
' `-' Original Submission
' ===============================================================
' CONTRIBUTOR: QBnut, Glenn, Dav
' DESCRIPTION: Glenn and Dav explain to QBnut how to make a QLB file.
' DATE POSTED: Mon Jan 7 23:36:01 2002
' ===============================================================


QBnut >> How do I make a qlb file???

Glenn >> LINK /Q FILE.LIB, FILE,, BQLB45;

Dav >> Glenns post is correct. You have to have the LINK.EXE file in 
the current directory or path.

What I do (because I'm lazy) is use a batch file so I don't have to 
type it in new every time. 

Here's the contents of my MAKEQLB.BAT batch file....

LINK /Q %1.LIB, %1.QLB, NUL, BQLB45;

So, if I had a LIB called GFX.LIB, and I want to make GFX.QLB, 
all I do is:

makeqlb GFX

and the QLB is created.

- Dav