                      ......................   
                     /                      \   
                    /         CASES          \
                   /   By Meech Productions   \
                  /  Reply to mooch23@msn.com  \
                  \----------------------------/

If the text is flying off the screen go to edit, then word wrap!


===============
 Introduction
===============

IF....THEN statements are very powerful and short to use in QuickBASIC. Cases are very helpful and can be used for more in depth, and complex decisions in QB. I personally think this way is more simpler and more organized to use, if you are making a big construction in your programs. Read on to find how to use cases yet, and whether you should use them or not in your programs!

==========================
 SELECT CASE...END SELECT
==========================

To start off using this statement, you must use the command SELECT CASE [variable]. The variable can be string or an integer. Then in the middle of the programs you have to have CASE [variable]. The variable is whatever the user inputs into qbasic. Then when you are finished you have to end it with END SELECT. Let's start with a simple example:

topp:
CLS 'clears the screen
PRINT " press one to start game"
PRINT " press two to end game"
INPUT, choice 
SELECT CASE choice
CASE 1
Print "Okay doke, let's get this game started already!"
GOTO label
CASE 2
PRINT "Leaving so soon? ):"
END
CASE ELSE
PRINT "That wasn't on the list, please select one or two, it isn't that hard!"
GOTO topp
END SELECT

Very similar to IF...THEN statements, but easier to use eh? Maybe, maybe not. 
First off, you ask the user to input 1 or 2 in the variable 'choice'. Then you have to SELECT CASE [variable] for choice. Since there is only two choices off the menu, you would only have CASE 1 and CASE 2. In between CASE 1 and CASE 2 just programm as you would be regularly. Now what is so cool about CASE is that you can use CASE ELSE, which is a very handy feature if a user accidently or purposely presses another key that isn't a choice. if any other key is pressed than 1 or 2, it will do whatever you tell it to under CASE ELSE. Negative values can also apply here, for example you could also use CASE -15 if you liked. But what happens if you wanted to do multiple numbers in one CASE?

topp:
CLS
INPUT "Insert your test score, out of one hundred points", score#
percent# = (score#/100) * 100
SELECT CASE score#
CASE 0 TO 59 
PRINT "You recieved an F, "; percent# ;" percent."
SLEEP 1  
PRINT "You may want to look into getting a tutor!"
END
CASE 60 TO 69
PRINT "You recieved a D, "; percent# ;" percent."
SLEEP 1
PRINT "Trying studying a little harder, okay?"
END
CASE 70 TO 79
PRINT "You recieved a C, "; percent# ;" percent."
SLEEP 1
PRINT "Your grade is satisfactory, but practice a bit more and you will be PRINT "great!"
END
CASE 80 TO 89
PRINT "You recieved a B, "; percent# ;" percent."
SLEEP 1
PRINT "You did very good on your test."
CASE 90 TO 100
PRINT "You recieved an A, "; percent# ;" percent."
SLEEP 1
PRINT "That's excellent dude!!!"
END
CASE ELSE
PRINT "Uh...no, that's not a possible score."
GOTO topp
END SELECT

Not much of a difference, but this example shows how you can use multiple numbers so you dont have to make a case over and over again. All you have to do is put CASE [smaller number] TO [larger number] and that's all there is to it!. But what happens if you wanted to do the numbers 3, 5, and 9-13? Simple!
Just do this

CASE 3, 5, 9 TO 13

It's that easy.  Mathematical operators can also be used such as greater than or equal to. here is a list of them:

<....Less than
<=...Less than or equal to
>....Greater than
>=...Greater than or equal to
<>...Not equal
=....Equal

Go back to the test program, and add this at the end before CASE ELSE

CASE IS > 100
PRINT "I never gave any extra credit on the test!"
DO
LOOP WHILE INKEY$ = ""
GOTO topp
CASE IS < 0
PRINT "Do you have really low self esteem or were you just cheating on the test?"
DO
LOOP WHILE INKEY$ = ""
GOTO topp


If the score# the user inputted is greater than 100, it tells the user there was no extra credit. If the score was lower, it asks the user if it was cheating or has low esteem. Just make sure you have CASE IS, not just CASE, even though QBasic corrects it for you.

Remember, CASES don't just have to be used when you are asking the user to input something! You can use them when you draw a random number, if a character moves to a certain position, or whatever. And of course, you want to now how to use them with letters! This too, is not difficult at all to do. If you perhaps read my "Keys in Qbasic" tutorial you would already know how to do this, but here it is again!

CLS 'clears the screen
INPUT "Howdy, what's your name " name$
SELECT CASE name$
CASE "Jonathan" 
PRINT "Hello Jonathan, welcome to the club!"
END
CASE "Mary"
PRINT "Hello Mary, welcome to the club!"
END
CASE ELSE
PRINT "Hey! Your name is not on the list! SECURITY!"
END
END SELECT

Just remeber, when using letters, the variable in SELECT CASE must be a string$, and the CASE must be in quotations "". Now, by using cases and different numbers you could do any values from different ranges. Can you do this using different names and such? Nope, sorry. . . . . .JUST KIDDING!!! Surely you can! Just use the same command, "TO" and/or commas!!! very simple! lets try this for an example. Just erase the middle of your program and put this in instead:

CASE "Jonathan", "Mary", "Peter", "Will", "Barbara"
PRINT "Welcome to the club, "; name$ ;"!!!"
END
CASE ELSE 
PRINT "Ummm...."; name$ ;", I don't see you on the list....SECURITY!"
END

You could make proggraming in cases much more easier this way...But I still haven't explained where you would use "TO". TO is very helpful, it could be used to alphabetize things, and for dictionary tabs in programs!

CASE "a" TO "j"

If QBasic was to ask the user for a string, and the string was "c" or "dogs" or "igloos" it would fall under the that case! If the string was "juice" or "n" it wouldn't fall under this CASE. Practice a little more if you don't understand. Cases can also be words to when using this!

CLS'clears the screen
INPUT "What is your name "; name$
SELECT CASE name$
CASE "alfonso" to "jamima"
PRINT name$; " get in between alfonso and jamima"
END
CASE "kent" to "oakley"
PRINT name$; "get in between kent and oakley."
END
CASE "patterson" to "williams"
PRINT name$; "get in between patterson and williams"
END
CASE ELSE
PRINT "Oh, you must be that new exchange student."
END
END SELECT


Now, I know that wouldn't cover every single name, but that's a start. I'm sure you have the strength to customize that to your needs.

One last thing and I can call this a wrap! Again, you can use just about any type of variable in cases. But what happens if you wanted to use CASE using the directional keys, enter, space, etc.? This gets a little more tough. You can do this by using CHR$(variable) commands. If you have already read my keys tutorial, you will already know how to do this in a similar way...But here it is again for the people haven't read it or want to review it.

CLS'clear the screen
move$ = INKEY$
DO
SELECT CASE move$
CASE CHR$(0) + CHR$(72)'up
PRINT "Up Key!" 
CASE CHR$(0) + CHR$(80)'down 
PRINT "Down Key!"
CASE CHR$(0) + CHR$(75)'left 
PRINT "Left key!"
CASE CHR$(0) + CHR$(77)'right
"Right key!"
CASE CHR$(13) 
PRINT "Enter key!"
CASE CHR$(32) 
PRINT "Space bar!"      
LOOP
CASE ELSE
PRINT "You pressed a button that wasn't on the list, so the QBasic program PRINT "got confused and ended.
END
END SELECT


Now you can use CHR$ in select. You press up, it says you press up, you press down, it prints you press down. Press a key other than up,down, left, right, space, or enter and it will stop running. 

=========================
 Legal Notice/Conclusion 
=========================

See how useful cases can be? I sure hope that this lesson gave you a few helpful hints, and I sure hope you put this to use in your QB programming. I know I kind of left you hanging on the last example, and I'm not trying to do this shameless self-promotion dealy, but I dont feel explaining it into more in depth should be covered in this tutorial...if you are still stuck on it I reccomend you read my keys tutorial also located in QB4all.com in the downloads, then tutorial section. Thanks for reading all! And I sure hope my couple of <sarcasm> hilarious </sarcasm> jokes didn't scare you away...  

Comments and questions are very much welcome. Send them to the author at mooch23@msn.com! Thanks!

Now, feel free to print a copy of this, make origami, or feed it to your goat! But I do take some pride in my work, so you definetly may not make any profits out of this, or claim it as your own! Not that you would want to... If you would like to distribute this or use it on your site or whatever, just drop me an email, and I'll be sure to let you! Thanks!

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
This tutorial is a copyright of Meech Productions written on June 17, 2004. 