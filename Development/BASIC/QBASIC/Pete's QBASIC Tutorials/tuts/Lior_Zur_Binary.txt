'+--------------+-------------------------------+-----------+
'| * Tutorial   |  Lior Zur <zur@inter.net.il>  | 25/5/1997 |
'+--------------+-------------------------------+-----------+
'| Graphic Tutorial Number 1:                               |
'|                                                          |
'|   Saving and Loading Graphics In Binary Mode             |
'|                                                          |
'+----------------------------------------------------------+
'| For more excellent QB tutorials, demos, stuff and goodies|
'| please visit my homepage at:                             |
'|                                                          |
'|    http://www.geocities.com/SiliconValley/Heights/9246   |
'+----------------------------------------------------------+--+
'| !!!!!!!!!!!!!!!!!!!!!!!! IMPORTANT: !!!!!!!!!!!!!!!!!!!!!!! |
'|                                                             |
'| You can publish this file anywhere, as long as you haven't  |
'| made any change in this file. If you use any of this code   |
'| in your own program, please make sure to give me a credit.  |
'| Also: I don't allow to publish this file on a CD without    |
'| my permission.                                              |
'+-------------------------------------------------------------+

SCREEN 13 'of course
DIM a%(200) 'so it will start in 0, I think it's better.
            'also make sure it's integer

RANDOMIZE TIMER ' to allow randomized graphic demo

FOR x = 1 TO 10     'just generate randomize picture
 FOR y = 1 TO 10
  Col = INT(RND * 16)  'Pick randomized color between 0-15
  PSET (x, y), Col    'and put a pixel with that color.
 NEXT y
NEXT x

GET (1, 1)-(10, 10), a%'get the picture

'*** Saving the data into file ***
'
'   Now, you have to store the array's structure into a file.
' This can be done by BSAVE and BLOAD (to load), or by a simple
' For - Next loop that goes through the array elements,and records
' each element into file.
'   To load, you use a similar FOR-NEXT loop to read the data from the
' file and then put it in the array.

'*** Why are binary files special? ***
'
'   When Qbasic writes into regular text file, like
' OPEN "LIOR.DAT" FOR OUTPUT AS #1
' it just writes the number, the value, into it. So, I don't mind to put
' INTEGER type variable into the file, and then read it into a LONG type
' variable, for example.
'   However, there is a difference between
' the two types - I mean, how they're stored in the computer's memory:
' INTEGER, for example, requires two bytes, and
' LONG INTEGER requires four bytes.
' That's why a long integer can contain much larger numbers than a
' regular integer. When we open a file as binary, and then write into
' it, we don't write a value, but we write the bytes of the variable.
' EXAMPLE:
'  You've wrote into binary file an integer: it wrote in two bytes.
'  But, when loading, you tried to load a long integer from the file:
'  the computer will try to get four bytes, but it will have only two.
'  -- ERROR !!--
' So, to make sure no errors will happen, we have to declare every
' data we want to save into a binary file. And then, when loading, use
' the same data types.

'*** What's wrong with BSAVE and BLOAD? ***
'
'  - For more info about BSAVE and BLOAD, please go to my page,
'    and download the second tutorial.
'
' BSAVE and BLOAD are commands that usually used by Qb programmers
' in order to save and load arrays into files. These commands are
' fast, and that's why people love to use them. BUT - you can't
' save any other info about the pictures in the same file, and you
' can't save more than 1 array into one file. When you open the
' file yourself and put in the information, you can take care of
' that kind of stuff. But in this method there is also a problem:
' loading is pretty slow, because of the FOR - NEXT loop. That's
' why M/K productions wrote a nice utility to load arrays from
' binary file. You can download it from my page:
' ** http://www.geocities.com/SiliconValley/Heights/9246/ **
' You can also see there how I used this utility to load a QBdraw
' file very fast. (0 seconds in my computer) That's why I
' recommend you to use the second method of the FOR-NEXT:
' * A bit slow saving, but very fast loading.
' * You can handle errors in file.
' * You can save a group of arrays in one file.
' * You can save other necessary information.
' * You can be sure you load the right kind of
'   file by putting a signature in the start.
' * File size can be above 64K.



COLOR 15: LOCATE 3, 1: PRINT "PRESS A KEY TO SAVE..."
G$ = INPUT$(1) 'Wait for one key press. Don't use SLEEP- it sucks!

num = FREEFILE 'this function gives you a number of file that is not
               'used yet.

'** Saving array:
OPEN "LIOR.PIC" FOR BINARY AS #num
FOR I = 0 TO 200    '0 to array size
 PUT #num, , a%(I)  'remember - we've used integer
NEXT I
CLOSE #num


CLS
COLOR 15: LOCATE 3, 1: PRINT "PRESS A KEY TO LOAD..."
G$ = INPUT$(1)

'** loading array
' I have used here the old and slow way, but from my homepage you
' can download a new FAST one to do the same thing.

OPEN "LIOR.PIC" FOR BINARY AS #num
FOR I = 0 TO 200    '0 to array size
 GET #num, , a%(I)    'we will just use the same old array, but you can
NEXT I                'use any array you wish (that's in the right size
CLOSE #num            'and data type, of course.)

PUT (1, 1), a%, PSET'put the picture that restored in the array

