
                         Scrolling Credits!!

                         x.t.r.GRAPHICS (TM)
                              Tutorial

-----------------------------------------------------------------------

    How do you make your credit screen scroll from the bottom of the
screen and up off the top. Forget using a LOCATE statment that moves
the names up the screen, because once they run off the top you get a
error.
    The way I found to do this is to set a LOCATE on 22, 1. Then I
place PRINT & SLEEP statments under it. The PRINT statments are then
forced to push each other up the screen. Example:

'## Start Here ###

LOCATE 22, 1
PRINT "                 Credits"  ' Use spacing to center text.
SLEEP (1)                         ' SLEEP Coltrols the speed.
PRINT                             ' Vertical space, makes it read easy.
SLEEP (1)
PRINT "       Look Ma', I'm scrolling up!"
SLEEP (1)
PRINT
SLEEP (1)
PRINT        ' Each PRINT pushes the next one up, place more to scoll
SLEEP (1)    '  higher up the screen.
PRINT
SLEEP (1)
PRINT

'## Stop Here ###

    This only goes part the way up the screen. This works in all
screens. Screen 9 is clearer, use this for large credits. 
 Screen 13 is easy to read and best for small credits. Now to make it
organized. I'll do another example with a profesional set up style:

'## Start Here ###
SCREEN 9
LOCATE 22, 1
PRINT "                                    Credits"
SLEEP (1)
PRINT
SLEEP (1)
PRINT
SLEEP (1)
PRINT "                         Programmer(s): Programmer 1"
SLEEP (1)
PRINT "                                        Programmer 2"
SLEEP (1)
PRINT "                                        Programmer 3"
SLEEP (1)
PRINT
SLEEP (1)
PRINT
SLEEP (1)
PRINT "                     Graphic Artist(s): Artist 1"
SLEEP (1)
PRINT "                                        Artist 2"
SLEEP (1)
PRINT "                                        Artist 3"
SLEEP (1)
PRINT
SLEEP (1)
PRINT
SLEEP (1)
PRINT "                                 Audio: Programmer 1"
SLEEP (1)
PRINT "                                        Programmer 2"
SLEEP (1)
PRINT "                                        Programmer 3"
SLEEP (1)
PRINT
SLEEP (1)
PRINT
SLEEP (1)
PRINT "                         Game Designer: Designer"
SLEEP (1)
PRINT
SLEEP (1)
PRINT
SLEEP (1)
PRINT "       Quality Assurance(or Debugging): Debuger"
SLEEP (1)
PRINT
SLEEP (1)
PRINT
SLEEP (1)
PRINT "                        Special Thanks: Person 1"
SLEEP (1)
PRINT "                                        Person 2"
SLEEP (1)
PRINT "                                        Person ..."
SLEEP (1)
PRINT
SLEEP (1)
PRINT
SLEEP (1)
PRINT
SLEEP (1)
PRINT
SLEEP (1)
PRINT
SLEEP (1)
PRINT
SLEEP (1)
PRINT
SLEEP (1)
PRINT

'## Stop Here ###

    I'd copy that to QBasic to save time. Oh and remember if you are 
wondering why I use LOCATE 22, 1 and not LOCATE 25, 1. Well, if you
use 25, the first line printed stays there and doesn't get pushed up.
You can try 23 & 24 and see if they work, I just use 22.
    It takes just a little debugging to find the center of each srceen,
but once you get the first one centered, you can use it as a guide 
line. Remember: The more SLEEP PRINT statments at the end pushes the
credits higher until they go off the top of the screen.
    I hope this was useful. Have fun makeing a scrolling credits
screen.

EMAIL: Rattrapmax6@aol.com
WEBPAGE: http://hometown.aol.com/rattrapmax6/index.html