GNU Chess 6.2.4
Copyright (C) 2016 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Usage: gnuchess [OPTION]...

Play the game of chess

Options:
 -h, --help         display this help and exit
 -v, --version      display version information and exit
 -q, --quiet        make the program silent on startup
     --silent       same as -q

 -x, --xboard       start in engine mode
 -p, --post         start up showing thinking
 -e, --easy         disable thinking in opponents time
 -m, --manual       enable manual mode
 -u, --uci          enable UCI protocol (externally behave as UCI engine)
 -M size, --memory=size   specify memory usage in MB for hashtable
 -a filename, --addbook=filename   compile book.bin from pgn book 'filename'
 -g, --graphic      enable graphic mode

 Options xboard and post are accepted without leading dashes
 for backward compatibility.

 Moves are accepted either in standard algebraic notation (SAN) or
 in coordinate algebraic notation.

 The file 'gnuchess.ini' allows setting config options. See
 'info gnuchess' for details. The file will be looked up in the current
 directory or, if not found there, in the directory pointed to by
 environment variable GNUCHESS_PKGDATADIR.

Report bugs to <bug-gnu-chess@gnu.org>.

