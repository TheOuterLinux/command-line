NAME

    This script uses text-browser to query and render Wikipedia
    articles. The output will be printed to standard out.

SYNOPSIS
    wikipedia2text [-BCnNoOpPsSuU] [-b prog] [-c patt] [-i patt] [-l lang] [-X browseroptions] query
    wikipedia2text -o [-b prog] [-l lang] query
    wikipedia2text [-h]
    wikipedia2text -v|-r

    -n	do not colorize			    -N	simple colorization (alias -C)
    -p	display using a pager		    -P  don't use pager
    -o	open Wikipedia article in browser   -O	don't open in browser
    -s	display only a summary		    -S	display whole article
    -u	Just output the query URL	    -U	open URL in browser
    -v	display version			    -h	display help

    -r                    open Random Page
    -i patt               colorize pattern (case insensitive)
    -I patt               colorize pattern (case-sensitive, alias -c)
    -b prog               use prog as browser (by default to invoke elinks, links2,
                          links, lynx or w3m, if found)
    -l lang               use language (currently supported are: af, als, ca, cs, da,
                          de, en, eo, es, fi, fr, hu, ia, is, it, la, lb, nds, nl, nn,
                          no, pl, pt, rm, ro, simple, sk, sl, sv, tr)
    -W url                use url as base-URL for wikipedia (e.g. use a different
                          Wiki, Querying this URL will happen by appending the search
                          term.
    -X "browseroptions"   pass through options to browser, e.g., "-width 180"
                          (warnings: must be in quotes; browser specific, not checked)

    Query can be any term to search for at Wikipedia. Special
    characters will be taken care of. Note that only one query term is
    supported, however this term can consist of one or more words.

    Configuration can also be controlled by creating a runcontrol file
    .wikipedia2textrc your home directory.

    Note that when requesting to open the article in a browser, other
    parameters will be ignored. The same holds for the options -h and
    -v.
