################
# Office Index #
################

abook - text-based address book program
aiksaurus - English-language thesaurus
ascii - ASCII character set encoded in octal, decimal, and hexadecimal
aspell - interactive spell checker
cal, ncal - displays a calendar and the date of Easter
calcurse - ncurses organizer
cat - concatenate files and print on the standard output
catdoc - reads MS-Word file and puts its content as plain text on 
         standard output
catppt - reads MS-PowerPoint file and puts its content on standard 
         output
dav - a minimalist ncurses-based text editor
dict - a client for the Dictionary Server Protocol (DICT)
ebook-polish - add/edit metadata within an EPUB; it is installed along
               with Calibre
echo - display a line of text
efax - send/receive faxes with Class 1, 2 or 2.0 fax modem
fbgs - Poor man's PostScript/pdf viewer for the linux 
       framebuffer console
gitview - an ASCII/HEX file viewer
gnuplot - an interactive plotting program
hledger-ui - curses-style interface for the hledger accounting tool
hnb - Hierarchical notebook
iconv - convert text from one character encoding to another
less - opposite of more
look - check for an English word, from the dictionary, in case of 
       confusion, right from the shell
markdown - convert text to html
mcedit - internal file editor of GNU Midnight Commander
mdp - a command-line based markdown presentation tool
more - file perusal filter for crt viewing
nano - Nano's ANOther editor, an enhanced free Pico clone
nl - outputs the content of text file with lines numbered
pandoc - general markup converter
patat - Presentations Atop The ANSI Terminal
poppler-utils - PDF utilities (based on Poppler)
pr - convert text files for printing
presentty - console-based presentation software
rubber - a building system for LaTeX documents
sc - spreadsheet calculator
script - make typescript of terminal session
sed - stream editor for filtering and transforming text
soffice - use --headless flag to edit/convert documents; comes with 
          LibreOffice or OpenOffice
spell - GNU spell, a Unix spell emulator
tac - concatenate and print files in reverse
tee - read from standard input and write to standard output and files
tesseract - command-line OCR engine
touch - change file timestamps
tpp - Text Presentation Program
tudu - a command-line tool to manage TODO lists hierarchically
txt2html - convert plain text file to HTML
txt2man - convert flat ASCII text to man page format
unhtml - strip the HTML formatting from a document or the standard input
         stream and display it to the standard output
unoconv - convert any document from and to any LibreOffice supported 
          format
when - a minimalistic personal calendar program
wikipedia2text - displays Wikipedia entries on the command line
wordgrinder - console-based word processor
wyrd - a text-based front-end to remind(1), a sophisticated calendar and
       alarm program
xlsx2csv - convert xlsx xml files to csv format
