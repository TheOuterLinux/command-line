###############
# cat - Basic #
###############

When you need to view a plain text file, 'cat' is the quickest option.
Cat can also view pretty much anything; just know that binaries and images
will look like random garbage. A lot of formats such as RTF, DOC, XLS,
etc. will too unless they have notes or meta-data attached to them. If
you are trying to view a PDF in TTY/console, try using 'fbgs' instead.

Basic usage:

    cat "/path/to/file.txt"
