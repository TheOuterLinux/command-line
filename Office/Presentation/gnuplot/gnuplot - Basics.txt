########################
# gnuplot - Basics.txt #
########################

01 | Getting started
   `----------------

    Start gnuplot:
    -------------

        gnuplot
    
    List modes:
    ----------

        set terminal
    
    Setup for TTY/console use:
    -------------------------

        set terminal dumb
    
02 | Examples
   `---------
    
    Plot a sine graph:
    -----------------

        plot sin(x)
    
    Replot above example to a PNG file:
    ----------------------------------
    
        set output "sinExample.png"
        replot
        
    Adding labels:
    -------------
    
        set xlabel "Time (s)"
        set ylabel "Force (lb)"
        plot sin(x)
        
    Formula examples:
    ----------------
    
        sqrt(2*pi)  --> Square root of 2π
        3*x**2+2    --> Same as (3x^2)+2
        
        
    
| Greek Symbols
`--------------

symbol    html4             latex           gnuplot

α         &alpha;          \alpha           {/Symbol a}
β         &beta;           \beta            {/Symbol b}
χ         &chi;            \chi             {/Symbol c}
Χ         &Chi;            \Chi             {/Symbol C}
δ         &delta;          \delta           {/Symbol d}
Δ         &Delta;          \Delta           {/Symbol D} 
ε         &epsilon;        \epsilon         {/Symbol e}
Ε         &Epsilon;        \Epsilon         {/Symbol E}
φ         &phi;            \phi             {/Symbol f}
Φ         &Phi;            \Phi             {/Symbol F}
γ         &gamma;          \gamma           {/Symbol g}  
Γ         &Gamma;          \Gamma           {/Symbol G}
η         &eta;            \eta             {/Symbol h}
Η         &Eta;            \Eta             {/Symbol H}
ι         &iota;           \iota            {/Symbol i}
Ι         &Iota;           \Iota            {/Symbol I}
κ         &kappa;          \kappa           {/Symbol k}
Κ         &Kappa;          \Kappa           {/Symbol K}
λ         &lambda;         \lambda          {/Symbol l}
Λ         &Lambda;         \Lambda          {/Symbol L}
μ         &mu;             \mu              {/Symbol m}
Μ         &Mu;             \Mu              {/Symbol M}
ν         &nu;             \nu              {/Symbol n}
Ν         &Nu;             \Nu              {/Symbol N}
ο         &omicron;        \omicron         {/Symbol o}
Ο         &Omicron;        \Omicron         {/Symbol O}
π         &pi;             \pi              {/Symbol p}
Π         &Pi;             \Pi              {/Symbol P}
θ         &theta;          \theta           {/Symbol q}  
Θ         &Theta;          \Theta           {/Symbol Q}
ρ         &rho;            \rho             {/Symbol r}
Ρ         &Rho;            \Rho             {/Symbol R}
σ         &sigma;          \sigma           {/Symbol s}
Σ         &Sigma;          \Sigma           {/Symbol S}
τ         &tau;            \tau             {/Symbol t}
Τ         &Tau;            \Tau             {/Symbol T}
υ         &upsilon;        \upsilon         {/Symbol u}
Υ         &Upsilon;        \Upsilon         {/Symbol U}
ω         &omega;          \omega           {/Symbol w}
Ω         &Omega;          \Omega           {/Symbol W}
ξ         &xi;             \xi              {/Symbol x}
Ξ         &Xi;             \Xi              {/Symbol X}
ψ         &psi;            \psi             {/Symbol y}
Ψ         &Psi;            \Psi             {/Symbol Y}
ζ         &zeta;           \zeta            {/Symbol z}
Ζ         &Zeta;           \Zeta            {/Symbol Z}
