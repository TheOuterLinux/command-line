MDP(1)                               General Commands Manual                               MDP(1)

NAME
       mdp - A command-line based markdown presentation tool

SYNOPSIS
       mdp [OPTION]... [FILE]

DESCRIPTION
       mdp  is a command-line program that allows you to make elegant presentations from markdown
       formatted FILEs.

       It is as easy as write your presentation content in the text editor of your preference and
       launch the presentation from the command-line.

OPTIONS
   Input Control
       FILE   The  input file from which the presentation is read. If no file is specified, or if
              the file name is -, the presentation is read from standard input.

   Output Control
       -e, --expand
              Enable character entity expansion (e.g. '&gt;' becomes '>').

       -f, --nofade
              Disable color fading in 256 color mode.

       -i, --invert
              Swap black and white color.

       -t, --notrans
              Disable transparency in transparent terminal.

   Miscellaneous Options
       -d, --debug
              Enable debug messages on STDERR. Add multiple times to increases debug level.

       -h, --help
              Display usage message and exit.

       -v, --version
              Display version and license information.

ENVIRONMENT VARIABLES
   Output Control
       MDP_LIST_HEAD[1-3], MDP_LIST_OPEN[1-3]
              Controls the list characters of unordered lists.

              The default is equivalent to:
              MDP_LIST_OPEN1=' |  '
              MDP_LIST_OPEN2=' |  '
              MDP_LIST_OPEN3=' |  '
              MDP_LIST_HEAD1=' +- '
              MDP_LIST_HEAD2=' +- '
              MDP_LIST_HEAD3=' +- '

MARKDOWN FORMATTING
       For a complete list of supported markups, refer the sample presentation  (sample.md)  pro‐
       vided alongside mdp, or online available at https://github.com/visit1985/mdp.

   Slides
       The  input  FILE  is split into multiple slides by horizontal rules. Each consisting of at
       least 3 --- or *** characters on a single line.  This line must be  prefixed  by  an  com‐
       pletely empty line.  It can also contain spaces but no other characters.

       If  any  slide is too large to fit into your current screen, an error message will be dis‐
       played at the moment the presentation is launched.

   Line-by-Line Mode
   Block-by-Block Mode
       A single <br>, <BR> or ^ on an otherwise empty line signals mdp to stop output of the cur‐
       rent slide (stop point) and wait for a key-press by the user.

       This  enables the user to display bullet points or list items one by one (line by line) or
       block by block.

   Headers
       mdp supports header lines in the format of @[DESCRIPTION] [VALUE]  The  first  two  header
       lines are displayed as title and author in top and bottom bar.

       Headers are only recognized at the top of the input FILE.

   Line spanning markup
       Supported are headlines, code blocks, quotes and unordered lists.

   In-line markup
       As well as bold text, underlined text and in-line code.

COLOR SUPPORT
       Most  terminals are able to display 256 colors these days. But some of them enable only 16
       colors by default. To enjoy mdp's full capabilities, these terminals need to  be  signaled
       to enable 256 color mode. This is usually done by setting the TERM environment variable.

       export TERM=xterm-256color

KEYBOARD CONTROLS
       h, j, k, l, Arrow keys, Space, Enter, Backspace, Page Up, Page Down
              Display next/previous slide or continue after a stop point.

       g, Home
              Jump to first slide.

       G, End Jump to last slide.

       1..N   Jump to Nth slide.

       r      Reload the input FILE. This key is disabled if input was read from standard input.

       q      Exit mdp.

AUTHOR
       Written  by  Michael  Goehler  and  others, see https://github.com/visit1985/mdp/blob/mas‐
       ter/AUTHORS.

COPYRIGHT
       Copyright (C) 2016 Michael Goehler

       This is free software; see the source for copying conditions. There is  NO  warranty;  not
       even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

User Commands                               2016-04-02                                     MDP(1)
