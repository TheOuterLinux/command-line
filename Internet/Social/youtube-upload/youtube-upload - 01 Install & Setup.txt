From: https://www.cnx-software.com/2014/02/09/how-to-upload-youtube-videos-with-the-command-line-in-linux/

How to Upload YouTube Videos with the Command Line in Linux


Like many people, I access Internet via an ADSL connection at home. ADSL stands
for “Asymmetric Digital Subscriber Line”, with Asymmetric being the key word
here, as it just mean your download speed will be (much) higher than your
upload speed. My ISP promises a theoretical 10 Mbps download speed, and 512
Kbps upload speed, and this is pretty close in reality: Data Rate: 10240
(downstream), 509 (upstream) kbps. Assuming a 265MB video, in the very best
case (63 KB/s), it would take 1 hour and 12 minutes to upload a video to
YouTube, but in practice it’s often closer to 2 or 3 hours.

Youtube_terminalIf it’s a video you’ve shot yourself, and copied inside your
computer, there’s very little you can do, except processing the video with
tools such as HandBrake to make it smaller before upload. But if the video
files are located somewhere in the Internet, and you happen to have access to a
VPS or dedicated server, you could consider uploading the video from the
command line using your server. A Youtube Upload script could also be useful to
automatize video uploads from a CCTV camera for example.

A Google search quickly found a script to do just that. Yeess! The Python
script youtube-upload can be downloaded and installed to upload videos to
YouTube from the command line. Unfortunately, it does not seem to work anymore.
My upload scenario is a little complex to handle as I’ve tried to login in a
different country (the server is based in the US, whereas I usually login from
Asia), and Google found this to be dodgy, sending me both an email and an SMS
to tell me about a “Suspicious sign in prevented”. But even as I tried on my
local machine, I got an HTTP 403 (Forbidden) reply.

Time for plan B. Google happens to have its own upload_video.py script, and I
could make it work with some, or rather a lot of, efforts mainly because of
authentication. There’s probably a better way (comments welcome), but here’s
what I had to do in Ubuntu 12.04 / 13.10:

 1. Download the script:

        1 mkdir youtube-upload
        2 cd youtube-upload
        3 wget https://github.com/youtube/api-samples/raw/master/python/upload_video.py

 2. Download and extract Google API Python Client, and move relevant
    directories to the directory where the script is located:

        1 pip install --upgrade google-api-python-client

 3. In a computer with a Desktop interface, create a project called “Youtube
    Upload Script” with Google Cloud Console (the name and id does not matter)
    using the YouTube credential, and
      □ Enable YouTube Data API v3 in APIs & auth->API.
      □ In Credentials, click on CREATE NEW CLIENT ID, select Installed
        application for Application Type, and Other for Installed application
        type, and click Create Client ID.
      □ You should now have a Client ID for native application that looks like:
        Youtube_Native_Client_ID
        
 4. Back to the command line, create a file client_secrets.json vi your
    preferred editor (e.g. nano, vi, …), with the Client ID, Client secret, and
    redirect URIs above:

        1 {
        2   "installed": {
        3     "client_id": "xxxxxxxxxx-yyyyyyyyyyyyyyy.apps.googleusercontent.com",
        4     "client_secret":"ABCDXXxxxxxxxxx-CddddddddD",
        5     "redirect_uris": ["http://locahost", "urn:ietf:wg:oauth:2.0:oob"],
        6     "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        7     "token_uri": "https://accounts.google.com/o/oauth2/token"
        8   }
        9 }

 5. Now you can upload a video to your YouTube account with the script. There
    are two cases:
      □ You have access to a desktop browser such as Chromium or Firefox on the
        machine where the script is executed:

            1 python upload_video.py --file=video.webm --title="Video Title" \
            2 --description="Video Description" --keywords="keyword1, keyword2" \
            3 --category=22 --privacyStatus="unlisted"


        Most options are self-descriptive. Category 22 refers to People and
        Blog. other categories available are: Comedy “23”, Education “27”, 
        Entertainment “24”,   Film & Animation “1”,  Gaming “20”,  Howto &
        Style “26”,  Music “10”, News & Politics  “25”,  Nonprofits & Activism
        “29”, People & Blogs  “22”,  Pets & Animals “15”,  Science & Technology
        “28”,  Sports “17”, and   Travel & Events “19”.

        privacyStatus lets you choose between unlisted, private or public.

      □ The machine where the script is executed does not have access to a
        desktop browser, and you need to use a browser in another machine, run
        the following command instead:

            1 python upload_video.py --file=video.webm --title="Video Title" \
            2 --description="Video Description" --keywords="keyword1, keyword2" \
            3 --category=22 --privacyStatus="unlisted" <span style="color: #008000;">--noauth_local_webserver</span>


        It will provide a link to access in your browser, to get a verification
        code that you can enter in the terminal. This is only requires for the
        first time, after you can just upload the videos without user
        intervention for authentication.
        If you run the command without noauth_local_webserver, and if you only
        have access to a text based web browser this won’t work, as they don’t
        usually support JavaScript.
        
 6. That’s it, the video should now upload to your YouTube account.

        1 Authentication successful.
        2 Uploading file...
        3 Video id '64sT_voVhhY' was successfully uploaded.

I’ve tested it with one of FOSDEM 2014 videos, namely Grate Liberating the
Tegra GPU with 265 MB of data, and instead of the 2 or 3 hours it would have
taken on my home computer, it only took 31 seconds in a VPS…

One issue with this method is that client_secrets.json can not be distributed,
as it contains your API credential, but if you want to create a script that
easily usable by others, there are other methods which I haven’t tried.

Support CNX Software - Donate via PayPal or become a Patron on Patreon
