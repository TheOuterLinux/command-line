################################
# Toxic - Install Instructions #
################################

These instructions assume that you know a little bit about the command-line.

Install:
-------

libpng12-dev
libpng16-16
libconfig-dev
libopenal-dev
libalut-dev
libnotify-dev
libvpx-dev
libqrencode-dev
libsodium-dev
libnacl-dev
libopus-dev
libcurl4-openssl-dev

Possibly need to install:
------------------------
vpx-tools
qrencode

Run:

[Unpack c-toxcore-master]
    cd /path/to/c-toxcore-master
    cmake .
    make
    sudo make install
    
[Unpack toxic-master]
    cd /path/to/toxic-master
    make
    sudo make install
    
Correct missing libtoxcore.so.2 issue:
-------------------------------------
    echo '/usr/local/lib' | sudo tee -a /etc/ld.so.conf.d/locallib.conf
    sudo ldconfig
