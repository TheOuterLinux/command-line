#####[ w3m -shell ]#####
You can quickly access the shell you launched w3m from by using "!".
Afterwards, you can also pull up a list of variables to do commands on
pertaining to the current page you are on in w3m by using 'env | sort'.

$W3M_URL             Current URL of the page before launching shell
$W3M_CURRENT_LINK    Current link the cursor was on
$W3M_CURRENT_IMG     Current image link the cursor was on

So on and so forth...

This will keep you from having to add or edit an external browser item
for rare instances.
